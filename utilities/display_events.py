import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import matplotlib.pyplot as plt
import numpy as np
import itertools
import utilities.pixmaps as pixmaps

Cam1_fill_pixel = np.array([[0.72653], [0.74653]])
Cam2_fill_pixel = np.array([[1.01715], [1.02715]])

Padded_Cam1_mc_pixmap = np.hstack((pixmaps.Cam1_mc_pixmap, Cam1_fill_pixel))
Padded_Cam1_data_pixmap = np.hstack((pixmaps.Cam1_data_pixmap, Cam1_fill_pixel))

Padded_Cam2_mc_pixmap = np.hstack((pixmaps.Cam2_mc_pixmap, Cam2_fill_pixel))
Padded_Cam2_data_pixmap = np.hstack((pixmaps.Cam2_data_pixmap, Cam2_fill_pixel))


def cam1_hex_event_figure(cam1_images, montecarlo):
    """
    returns a figure containing all Cam1 telescope images

    Parameters:
        cam1_images: Numpy array of shape [4, 960]
        Can be omitted via None in order to fill with zeros

        montecarlo: Boolean
        Indicates which pixmap to use
    """

    pixmap = Padded_Cam1_mc_pixmap if montecarlo else Padded_Cam1_data_pixmap
    cam1_images = np.lib.pad(cam1_images, ((0, 0), (0, 1)), 'constant')

    figure, axes = plt.subplots(2, 2)
    axes = list(itertools.chain.from_iterable(axes))  # flatten 2x2 axes array into 1x4

    for ax, image in zip(axes, cam1_images):
        ax.set_xlim(-0.69, 0.69)
        ax.set_ylim(-0.67, 0.67)
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        ax.set_aspect('equal')
        ax.set_axis_off()
        ax.hexbin(x=pixmap[0], y=pixmap[1],
                  C=image, bins='log', gridsize=33)

    figure.subplots_adjust(hspace=0.01, wspace=0.01, left=0.01, right=0.99, top=0.99, bottom=0.01)
    return figure


def cam2_hex_event_figure(cam2_image, montecarlo):
    """
    returns a figure containing the Cam2 telescope image

    Parameters:
        cam2_image: Numpy array of shape [1, 2048]
        Can be omitted via None in order to fill with zeros

        montecarlo: Boolean
        Indicates which pixmap to use
    """

    pixmap = Padded_Cam2_mc_pixmap if montecarlo else Padded_Cam2_data_pixmap
    cam2_image = np.lib.pad(cam2_image, ((0, 0), (0, 1)), 'constant')

    figure, ax = plt.subplots(1, 1)
    ax.set_xlim(-1.02, 1.02)
    ax.set_ylim(-0.96, 0.95)
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_aspect('equal')
    ax.set_axis_off()
    ax.hexbin(x=pixmap[0], y=pixmap[1],
              C=cam2_image[0], bins='log', gridsize=48)

    figure.subplots_adjust(hspace=0.01, wspace=0.01, left=0.01, right=0.99, top=0.99, bottom=0.01)
    return figure


def cam1_square_event_figure(cam1_images):
    """
    returns a figure containing all preprocessed Cam1 telescope images

    Parameters:
        cam1_images: Numpy array of shape [4, x_res, y_res]
    """

    figure, axes = plt.subplots(2, 2)
    axes = list(itertools.chain.from_iterable(axes))  # flatten 2x2 axes array into 1x4

    for ax, image in zip(axes, cam1_images):
        ax.imshow(image.T, origin='lower', aspect='equal')

    figure.subplots_adjust(hspace=0.01, wspace=0.01, left=0.01, right=0.99, top=0.99, bottom=0.01)
    return figure


def show_events_from_file(file_path, rebin_resolution):
    label_to_type = {0: 'real', 1: 'gamma', 2: 'proton', 3: 'electron'}

    import input_pipeline.image_preprocessing as pp
    with h5py.File(file_path, 'r') as f:
        total_events = f['time'].size
        for eventIdx in range(total_events):
            figure, axes = plt.subplots(2, 4)
            axes = list(itertools.chain.from_iterable(axes))
            hex_axes = [axes[i] for i in [0, 1, 4, 5]]
            squ_axes = [axes[i] for i in [2, 3, 6, 7]]

            pixmap = Padded_Cam1_mc_pixmap
            cam1_images = np.lib.pad(f['cam1_images'][eventIdx], ((0, 0), (0, 1)), 'constant')

            for ax, image in zip(hex_axes, cam1_images):
                ax.set_xlim(-0.69, 0.69)
                ax.set_ylim(-0.67, 0.67)
                ax.set_xticklabels([])
                ax.set_yticklabels([])
                ax.set_aspect('equal')
                ax.set_axis_off()
                ax.hexbin(x=pixmap[0], y=pixmap[1],
                          C=image, bins='log', gridsize=33)

            square_image = pp.preprocess_cam1(f['cam1_images'][eventIdx][None, :], 'rebin', 32).squeeze()
            for ax, image in zip(squ_axes, square_image):
                ax.imshow(image.T, origin='lower', aspect='equal')

            figure.suptitle('Label: {}'.format(label_to_type[f['label'][eventIdx]]), fontsize=20)
            plt.show()

def show_events_from_np():
    from input_pipeline.h5_generators import NumpyReader
    import os
    DATA_ROOT = os.environ['MASTERDATA']

    valid_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_valid.npz', requested_batches=np.inf,
                               batch_size=1, target='nominal')

    for img in valid_reader.generator():
        f, a = plt.subplots(2,2)
        a[0, 0].imshow(img[0][0,0])
        a[0, 1].imshow(img[0][0,1])
        a[1, 0].imshow(img[0][0,2])
        a[1, 1].imshow(img[0][0,3])
        plt.title(img[1])
        plt.show()

if __name__ == '__main__':
    #show_events_from_file('/home/obay/Dropbox/masterThesis/data/new_exports/classification/sim_real/train.h5', 32)
    show_events_from_np()
