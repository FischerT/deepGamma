import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import numpy as np
from time import time
import sys


def get_h5_as_dict(file_path):
    with h5py.File(file_path, 'r') as f:
        data = {
            'IDs': f['IDs'][:],
            'cam1_Images': f['cam1_Images'][:],
            'cam2_Images': f['cam2_Images'][:],
            'AltAz': f['AltAz'][:],
            'RaDec': f['RaDec'][:],
            'Energy': f['Energy'][:],
            'Size': f['Size'][:],
            'Time': f['Time'][:]
        }
    return data


def write_dict_to_h5(data, file_path, chunk_size=128):
    with h5py.File(file_path, 'w') as f:
        for name, value in data.items():
            chunks = (chunk_size,) + tuple(value.shape[1:])
            dataset = f.create_dataset(name=name, shape=value.shape,
                                       chunks=chunks, dtype=value.dtype,
                                       compression=32004, shuffle=True)
            dataset[:] = value
        f.create_dataset(name='mcFlag', data=True)


def split_dict(data, train_ratio=0.6, test_ratio=0.5):
    # splits the data dict into three dicts:
    # the two numbers specify the ratio of
    # train_ratio = train/all
    # test_ratio = test/(valid+test)
    # where valid+test=1-train/all

    total_events = data['Time'].size
    train_events = int(total_events * train_ratio)
    valid_events = int(total_events * (1 - train_ratio) * test_ratio)
    test_events = int(total_events * (1-train_ratio)*test_ratio)

    indices = np.arange(total_events)

    train_indices = indices[:train_events]
    valid_indices = indices[train_events: train_events + valid_events]
    test_indices = indices[train_events + valid_events:]

    train_data = {name: value[train_indices] for name, value in data.items()}
    valid_data = {name: value[valid_indices] for name, value in data.items()}
    test_data = {name: value[test_indices] for name, value in data.items()}

    return train_data, valid_data, test_data


def filter(data):
    size_cut = 0
    min_cam1_images = 2

    mask = np.count_nonzero(data['Size'][:, :4], axis=1) > 0
    print(np.sum(mask))
    for name, value in data.items():
        data[name] = data[name][mask]


def shuffle_dict(data):
    total_events = data['Energy'].size
    np.random.seed(0)
    indices = np.random.permutation(total_events)
    shuffled_dict = {name: value[indices] for name, value in data.items()}
    return shuffled_dict


def shuffle(gammas, protons):
    gamma_count = gammas['Energy'].size
    proton_count = protons['Energy'].size

    # count of events in smallest class, needed to balance the dataset
    class_count = min((gamma_count, proton_count))

    # shuffle gammas and protons separately
    gammas = shuffle_dict(gammas)
    protons = shuffle_dict(protons)

    # cut them to the same size
    gammas = {name: value[:class_count] for name, value in gammas.items()}
    protons = {name: value[:class_count] for name, value in protons.items()}

    # give them some labels
    gammas['Label'] = np.ones(class_count)
    protons['Label'] = np.zeros(class_count)

    gammas_train, gammas_valid, gammas_test = split_dict(gammas)
    protons_train, protons_valid, protons_test = split_dict(protons)

    both_train = {name: np.concatenate((gammas_train[name], protons_train[name])) for name in gammas.keys()}
    both_valid = {name: np.concatenate((gammas_valid[name], protons_valid[name])) for name in gammas.keys()}
    both_test = {name: np.concatenate((gammas_test[name], protons_test[name])) for name in gammas.keys()}

    both_train = shuffle_dict(both_train)
    both_valid = shuffle_dict(both_valid)
    both_test = shuffle_dict(both_test)

    return both_train, both_valid, both_test


if __name__ == '__main__':
    gamma_file = '/home/tfischer/Dropbox/masterThesis/data/gamma_diffuse.lz4_128.h5'
    train_file = '/home/tfischer/Dropbox/masterThesis/data/regression_train.lz4_128.h5'
    valid_file = '/home/tfischer/Dropbox/masterThesis/data/regression_valid.lz4_128.h5'
    test_file = '/home/tfischer/Dropbox/masterThesis/data/regression_test.lz4_128.h5'

    meta_start = time()

    start = time()
    gamma_data = get_h5_as_dict(gamma_file)
    print('Reading took {}s'.format(time()-start))

    start = time()
    filter(gamma_data)
    print('Filter took {}s'.format(time() - start))

    start = time()
    train_data, valid_data, test_data = split_dict(gamma_data)
    print('Splitting took {}s'.format(time() - start))

    start = time()
    write_dict_to_h5(train_data, train_file)
    print('Writing train took {}s'.format(time() - start))

    start = time()
    write_dict_to_h5(valid_data, valid_file)
    print('Writing vaild took {}s'.format(time() - start))

    start = time()
    write_dict_to_h5(test_data, test_file)
    print('Writing test took {}s'.format(time() - start))

    print('Everything took {}s'.format(time() - meta_start))
