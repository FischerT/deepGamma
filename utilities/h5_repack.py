import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import numpy as np

import os
import sys
import multiprocessing as mp
from itertools import repeat


def repack_to_LZ4(file_path, chunk_size, optimize_for_random_access=False):
    """
    Repack some HDF5 file from the exporter with LZ4 compression.+
    """

    names = [
        'IDs',
        'cam1_Images',
        'cam2_Images',
        'AltAz',
        'RaDec',
        'Energy',
        'Size',
        'Time'
    ]
    with h5py.File(file_path, 'r') as source_file:
        source_datasets = [source_file[name] for name in names]
        total_events = source_datasets[7].size
        mcFlag = source_file['mcFlag'].value

        slices = []
        old_index = 0
        for index in range(chunk_size, total_events, chunk_size):
            slices.append(slice(old_index, index))
            old_index = index
        slices.append(slice(old_index, old_index + (total_events % chunk_size)))

        with h5py.File(file_path + '_lz4_' + str(chunk_size), 'w') as destination_file:
            for name, source_dataset in zip(names, source_datasets):
                new_chunks = (chunk_size,) + tuple(source_dataset.chunks[1:])
                destination_dataset = destination_file.create_dataset(name=name, shape=source_dataset.shape,
                                                                      chunks=new_chunks, dtype=source_dataset.dtype,
                                                                      compression=32004, shuffle=True)
                for slc in slices:
                    destination_dataset[slc] = source_dataset[slc]

            destination_file.create_dataset(name='mcFlag', data=mcFlag)

if __name__ == '__main__':
    file_paths = sys.argv[2:]
    try:
        chunk_size = int(sys.argv[1])
        print("Chunk size: ", chunk_size)
    except:
        print("First argument must be integer chunk size!")
        quit()

    print('Processing the files:')
    for file_path in file_paths:
        print(file_path)

    with mp.Pool(len(file_paths)) as p:
        p.starmap(repack_to_LZ4, zip(file_paths, repeat(chunk_size)))
