import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import numpy as np
import os

pixmap_path = os.path.join(os.path.dirname(__file__), '..', 'pixmap.h5')

with h5py.File(pixmap_path, 'r') as pixmap_file:
    Cam1_data_pixmap = pixmap_file['cam1_pixmap_data'][:]
    Cam2_data_pixmap = np.zeros((2, 2048))  # we should figure this out some time soon TM

    Cam1_mc_pixmap = pixmap_file['cam1_pixmap_mc'][:]
    Cam2_mc_pixmap = pixmap_file['cam2_pixmap_mc'][:]


def data_to_mc_mapping():
    indices = []
    for data_pixel_coord in Cam1_data_pixmap.T:
        for idx, mc_pixel_coord in enumerate(Cam1_mc_pixmap.T):
            if np.allclose(mc_pixel_coord, data_pixel_coord):
                indices.append(idx)
                break
    return indices

if __name__ == '__main__':
    print(data_to_mc_mapping())