import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import numpy as np
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt
from pathlib import Path
import os

DATA_ROOT = os.environ['MASTERDATA']

class H5Check:
    def __init__(self, data_folder_path):
        path = Path(data_folder_path)
        if not path.exists():
            raise ValueError('Path does not exist!')

        if path.is_dir():
            self.file_paths = [file_path.as_posix() for file_path in path.glob('*.h5')]
        else:
            self.file_paths = [path.as_posix()]

        self.multiplicity = [0, 1, 2, 3, 4]
        self.size_cut = 0.
        self.loc_dis_cut = 100.

    def cut_indices(self, h5_file):
        triggered_telescope_count = np.sum((h5_file['cam1_size'].value > self.size_cut) &
                                           (h5_file['cam1_loc_dis'].value < self.loc_dis_cut), axis=1)
        bool_indices = np.isin(triggered_telescope_count, self.multiplicity)
        return bool_indices

    def total_events(self):
        total_events = 0
        for file_path in self.file_paths:
            with h5py.File(file_path, 'r') as f:
                total_events += np.sum(self.cut_indices(f))
        return total_events

    def plot_energy_distribution(self):
        energies = []
        for file_path in self.file_paths:
            with h5py.File(file_path, 'r') as f:
                energies.extend(f['energy'][:])

        bins = np.logspace(np.log10(min(energies)), np.log10(max(energies)), 500)
        plt.hist(energies, bins=bins, log=True)
        plt.gca().set_xscale('log')
        plt.show()

    def plot_nominal_source_pos(self):
        x_coords = []
        y_coords = []
        for file_path in self.file_paths:
            with h5py.File(file_path, 'r') as f:
                data = f['nominal'].value[self.cut_indices(f)]
                x_coords.extend(data[:, 0]/np.pi*180)
                y_coords.extend(data[:, 1]/np.pi*180)

        plt.hist2d(x_coords, y_coords, bins=100)
        plt.axis('equal')
        plt.show()

    def plot_cog(self):
        x_coords = []
        y_coords = []
        for file_path in self.file_paths:
            with h5py.File(file_path, 'r') as f:
                x_data = f['cam1_cog_x'].value[self.cut_indices(f), :]/np.pi*180
                y_data = f['cam1_cog_y'].value[self.cut_indices(f), :]/np.pi*180
                for cam in range(4):
                    x_coords.extend(x_data[:, cam][np.nonzero(x_data[:, cam])])
                    y_coords.extend(y_data[:, cam][np.nonzero(y_data[:, cam])])

        plt.hist2d(x_coords, y_coords, bins=100)
        plt.axis('equal')
        plt.show()

    def plot_loc_dis(self):
        loc_dis = []
        for file_path in self.file_paths:
            with h5py.File(file_path, 'r') as f:
                data = f['cam1_loc_dis'].value[self.cut_indices(f)]
                for cam in range(4):
                    loc_dis.extend(data[:, cam][np.nonzero(data[:, cam])])

        plt.hist(loc_dis, bins=100, log=True)
        plt.show()

    def plot_size(self):
        sizes = []
        for file_path in self.file_paths:
            with h5py.File(file_path, 'r') as f:
                data = f['cam1_size'].value[self.cut_indices(f)]
                for cam in range(4):
                    sizes.extend(data[:, cam][np.nonzero(data[:, cam])])

        bins = np.logspace(np.log10(min(sizes)), np.log10(max(sizes)), 100)
        plt.hist(sizes, bins=bins, log=True)
        plt.gca().set_xscale('log')
        plt.show()

    def image_sparsity(self):
        total_pixels = 0
        nonzero_pixels = 0
        for file_path in self.file_paths:
            with h5py.File(file_path, 'r') as f:
                data = f['cam1_images'].value[self.cut_indices(f)]
                triggered_images = f['cam1_size'].value[self.cut_indices(f)] > 0
                total_pixels += data[triggered_images].size
                nonzero_pixels += np.count_nonzero(data[triggered_images])

        return nonzero_pixels/total_pixels

if __name__ == '__main__':
    path = DATA_ROOT + 'gamma_ps_phase1'

    checka = H5Check(path)

    for x in range(5):
        checka.multiplicity = [x]
        print('Multi = %i ' % x, checka.total_events())


    checka.multiplicity = np.array([2,3,4], dtype=np.int64)
    print('Total ', checka.total_events())

    checka.plot_nominal_source_pos()
