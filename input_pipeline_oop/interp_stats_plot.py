import numpy as np
import matplotlib.pyplot as plt

data = np.load('interp_stats.npz')
images = data['images']
interp_images = data['interp_images']
rebin_images = data['rebin_images']

log_min = np.log10(np.min(np.abs(interp_images)))
log_max = np.log10(max(interp_images))

bins = np.logspace(log_min, log_max, 1000)

fig, axes = plt.subplots(1, 2)

axes[0].hist(interp_images, bins=bins, log=True, histtype='step', label='cubic')
axes[0].hist(-interp_images, bins=bins, log=True, histtype='step', label='negative cubic')
axes[0].hist(images, bins=bins, log=True, histtype='step', label='original')
axes[0].hist(rebin_images, bins=bins, log=True, histtype='step', label='rebin', )
axes[0].set_xscale('log')
axes[0].set_xlim([1e-63, 10**3.6])
axes[0].set_ylim([0.9, 1e6])
axes[0].set_xlabel('Pixel intensity in p.e.')
axes[0].set_ylabel('counts')

bins = np.logspace(log_min, log_max, 10000)

axes[1].hist(interp_images, bins=bins, log=True, histtype='step', label='cubic')
axes[1].hist(-interp_images, bins=bins, log=True, histtype='step', label='negative cubic')
axes[1].hist(images, bins=bins, log=True, histtype='step', label='original')
axes[1].hist(rebin_images, bins=bins, log=True, histtype='step', label='rebin')
axes[1].set_xscale('log')
axes[1].set_xlim([1e-3, 10**3.6])
axes[1].set_ylim([0.9, 1e5])
axes[1].set_xlabel('Pixel intensity in p.e.')
axes[1].set_ylabel('counts')

plt.legend(loc='upper left', ncol=4)

plt.show()
