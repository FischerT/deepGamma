import h5py
import matplotlib.pyplot as plt
import multiprocessing as mp
import numpy as np

from input_pipeline_oop.preprocessors import InterpolationPreprocessor, RebinPreprocessor

import time


def interpolate_file(file_path, num_events):
    with h5py.File(file_path, 'r') as f:
        images = f['cam1_images'][:num_events].reshape(-1, 960)
    print('total_images: ', images.shape[0])
    preproc = InterpolationPreprocessor(32, 'cubic')
    with mp.Pool(8) as p:
        start = time.time()
        interp_images = p.map(preproc.preprocess, images)
        total = time.time() - start
    print('total time cubic           : ', total)
    print('time per preproc           : ', total / images.shape[0])
    print('preproc per time           : ', images.shape[0] / total)
    print('preproc per time per thread: ', images.shape[0] / total / 8)
    preproc = RebinPreprocessor(32)
    start = time.time()
    rebin_images = preproc.preprocess_batch(images)
    total = time.time() - start
    print()
    print('total time rebin           : ', total)
    print('time per preproc           : ', total / images.shape[0])
    print('preproc per time           : ', images.shape[0] / total)
    print('preproc per time per thread: ', images.shape[0] / total / 8)
    images = images.ravel()
    interp_images = np.array(interp_images).ravel()
    rebin_images = rebin_images.ravel()
    print('Image density: ', np.nonzero(images)[0].size / images.size)
    print('interp density: ', np.nonzero(interp_images)[0].size / interp_images.size)
    print('rebin density: ', np.nonzero(rebin_images)[0].size / rebin_images.size)
    images = images[np.nonzero(images)]
    interp_images = interp_images[np.nonzero(interp_images)]
    rebin_images = rebin_images[np.nonzero(rebin_images)]

    return images, interp_images, rebin_images

file_path = '/home/oba/Dropbox/masterThesis/data/new_exports/PKS2155/run_048070_DST_001.h5'
#file_path = '/home/oba/Dropbox/masterThesis/data/new_exports/konrad_proton_diffuse/proton_20deg_180deg_run85275___phase2b5_desert-ws0.dst.h5'
#file_path = '/home/oba/Dropbox/masterThesis/data/new_exports/konrad_gamma_diffuse/gamma_20deg_180deg_run85068___phase2b5_desert-ws0_cone5.dst.h5'

images, interp_images, rebin_images = interpolate_file(file_path, 5000)

np.savez('interp_stats', images=images, interp_images=interp_images, rebin_images=rebin_images)

