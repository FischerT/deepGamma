import abc
import os

import numpy as np
from scipy.sparse import csr_matrix
import scipy.interpolate as sp

from scipy.interpolate import LinearNDInterpolator, CloughTocher2DInterpolator
import scipy.spatial.qhull as qhull

import Polygon.Shapes as ps
from Polygon.cPolygon import Polygon

import utilities.pixmaps as pixmaps
pixmap = pixmaps.Cam1_mc_pixmap

matrix_path_proto = os.path.join(os.path.dirname(__file__), 'preprocessing_cache',  'rebinMatrix_{}.npz')
square_path_proto = os.path.join(os.path.dirname(__file__), 'preprocessing_cache',  'squareGrid_{}.npz')


class PreprocessorBase(abc.ABC):
    @property
    @abc.abstractmethod
    def grid_coordinates(self):
        pass

    @abc.abstractmethod
    def preprocess(self, image_batch):
        pass


class RebinPreprocessor(PreprocessorBase):
    def __init__(self, x_resolution):
        self.rebin_matrix, self.resolution, self._grid_coordinates = self._get_rebin_matrix_and_resolution(x_resolution)

    @property
    def grid_coordinates(self):
        return self._grid_coordinates

    def preprocess(self, image):
        flat_preprocessed = self.rebin_matrix.dot(image)
        return flat_preprocessed

    def preprocess_batch(self, image_batch):
        flat_preprocessed_batch = self.rebin_matrix.dot(image_batch.reshape(-1, 960).T)
        return flat_preprocessed_batch.T.reshape(image_batch.shape[0], np.prod(self.resolution))

    @staticmethod
    def _calculate_rebin_grid(camera_pixels, scale):
        """
        Helper method to calculate a grid for the new square pixels.

        :param camera_pixels: list of polygons
        :return: square grid coords, square grid resolution
        """
        # get basic geometry
        bounding_boxes = np.array([np.array(p.boundingBox()) for p in camera_pixels])
        xmin, ymin = np.amin(bounding_boxes[:, 0::2], axis=0)
        xmax, ymax = np.amax(bounding_boxes[:, 1::2], axis=0)
        asp_box = (xmax - xmin) / (ymax - ymin)
        pixel_count = len(camera_pixels)

        # calculate square grid resolution based on scaling parameter
        xres = (np.rint(scale * np.sqrt(pixel_count * asp_box))).astype(int)
        yres = (np.rint(scale * np.sqrt(pixel_count / asp_box))).astype(int)

        # match the aspect ratio
        asp_grid = xres / yres
        if asp_box > asp_grid:  # box wider than grid
            factor = asp_box / asp_grid
            dy = (ymax - ymin) * (factor - 1)
            ymin -= dy / 2
            ymax += dy / 2
        else:  # box higher than grid
            factor = asp_grid / asp_box
            dx = (xmax - xmin) * (factor - 1)
            xmin -= dx / 2
            xmax += dx / 2

        # setup the square grid
        square_distance = (xmax - xmin) / xres
        square_distance *= 1.000000000000001  # numerical thing, yo know
        squares = np.mgrid[xmin:xmax:square_distance, ymin:ymax:square_distance].reshape(2, -1).astype(np.float64)

        # return centers of new pixels, not bottom left corner
        return squares + square_distance / 2, (xres, yres)

    def _calculate_rebin_matrix(self, x_resolution):
        # get camera plane geometry
        pixel_distance = pixmap[0, 1] - pixmap[0, 0]
        hexagon_radius = pixel_distance * np.tan(np.pi / 6)
        pixel = ps.Circle(radius=hexagon_radius, points=6)
        pixel_area = pixel.area()

        # create list of all camera pixel polygons
        camera_pixels = []
        for pos in np.transpose(pixmap):
            pix = Polygon(pixel)
            pix.shift(pos[0], pos[1])
            camera_pixels.append(pix)

        # brute force lots of scale factors to find one matching the resolution
        for scale in np.arange(1., 4., 0.01, dtype=np.float32):
            rebin_grid, resolution = self._calculate_rebin_grid(camera_pixels, scale)
            if resolution[0] == x_resolution:
                break
        else:
            raise ValueError("Could not find rebin matrix for resolution {}".format(x_resolution))

        square_distance = rebin_grid[1, 1] - rebin_grid[1, 0]

        # create list of all rebin pixel polygons
        square_pixels = []
        for coord in np.transpose(rebin_grid):
            square = ps.Rectangle(square_distance)
            square.shift(coord[0] - square_distance / 2, coord[1] - square_distance / 2)
            square_pixels.append(square)

        rebin_matrix = np.zeros((len(square_pixels), len(camera_pixels)))
        for idx in np.ndindex(rebin_matrix.shape):
            overlap = square_pixels[idx[0]] & camera_pixels[idx[1]]
            rebin_matrix[idx] = overlap.area() / pixel_area

        rebin_matrix = csr_matrix(rebin_matrix)

        matrix_path = matrix_path_proto.format(x_resolution)
        np.savez(matrix_path,
                 data=rebin_matrix.data,
                 indices=rebin_matrix.indices,
                 indptr=rebin_matrix.indptr,
                 resolution=resolution,
                 grid=rebin_grid)

    def _get_rebin_matrix_and_resolution(self, x_resolution):
        """
        If already computed, load rebin matrix and resolution. If not, compute!

        :param x_resolution: desired resolution of output images in x-direction
        :return: sparse rebin matrix, new image resolution
        """
        matrix_path = matrix_path_proto.format(x_resolution)
        if not os.path.exists(matrix_path):
            self._calculate_rebin_matrix(x_resolution)
        storage_dict = np.load(matrix_path)
        rebin_matrix = csr_matrix((storage_dict['data'],
                                   storage_dict['indices'],
                                   storage_dict['indptr']))
        return rebin_matrix, storage_dict['resolution'], storage_dict['grid']


class InterpolationPreprocessor(PreprocessorBase):
    def __init__(self, x_resolution, mode):
        self.mode = mode

        grid_path = square_path_proto.format(x_resolution)
        if not os.path.exists(grid_path):
            self._calculate_square_grid(x_resolution)
        storage_dict = np.load(grid_path)
        self._grid_coordinates = storage_dict['grid']
        self.resolution = storage_dict['resolution']

        fill_pixels = self._calculate_fill_pixels()
        self.fill_pixel_count = fill_pixels.shape[1]
        self.pixmap = np.concatenate((pixmap, fill_pixels), axis=1)

        self.delaunay = qhull.Delaunay(self.pixmap.T)

    @property
    def grid_coordinates(self):
        return self._grid_coordinates

    def preprocess(self, image):
        """

        :param image: Batch_size * Channels * 960 images
        :return:
        """
        image = np.pad(image, ((0, self.fill_pixel_count), ), mode='constant')

        if self.mode == 'linear':
            ip = LinearNDInterpolator(self.delaunay, image, fill_value=0.0)
            square_image = ip(self.grid_coordinates.T)
        elif self.mode == 'cubic':
            ip = CloughTocher2DInterpolator(self.delaunay, image, fill_value=0.0)
            square_image = ip(self.grid_coordinates.T)
        else:
            raise ValueError('interpolation method not supported! %s' % self.mode)

        return square_image.reshape(*self.resolution)

    @staticmethod
    def _calculate_fill_pixels():
        unique_y = np.sort(np.unique(pixmap[1]))
        even_y = unique_y[0::2]
        odd_y = unique_y[1::2]
        # get the according unique x coordinates
        even_x = []
        odd_x = []
        for pos in np.transpose(pixmap):
            if pos[1] in odd_y:
                odd_x.append(pos[0])
            else:
                even_x.append(pos[0])
        even_x = np.unique(even_x)
        odd_x = np.unique(odd_x)
        # create new grids, containing all pairs of coordinates
        even_grid = np.vstack(map(np.ravel, np.meshgrid(even_x, even_y)))
        odd_grid = np.vstack(map(np.ravel, np.meshgrid(odd_x, odd_y)))
        filled = np.append(even_grid, odd_grid, axis=1)
        # collect only new datapoints (aka fillers)
        grid_set = set(tuple(x) for x in np.transpose(pixmap[0:2]))
        filled_set = set(tuple(x) for x in np.transpose(filled))
        fillers = np.transpose(np.array([np.array(x) for x in grid_set ^ filled_set]))
        return fillers

    @staticmethod
    def _calculate_square_grid(x_resolution):
        dimensions = np.amax(pixmap, axis=1) - np.amin(pixmap, axis=1)
        asp = dimensions[0] / dimensions[1]
        pixel_count = pixmap.size / 2.
        # calculate square grid resolution based on scaling parameter

        for scale in np.arange(1., 4., 0.01, dtype=np.float32):
            xres = (np.rint(scale * np.sqrt(pixel_count * asp))).astype(int)
            if xres == x_resolution:
                break
        else:
            raise ValueError("Could not find rebin matrix for resolution {}".format(x_resolution))

        yres = (np.rint(scale * np.sqrt(pixel_count / asp))).astype(int)
        # setup the square grid
        squares = np.mgrid[0:xres, 0:yres].reshape(2, -1).astype(np.float64)
        # shift grid to center of mass = (0,0)
        squares -= np.mean(squares, axis=1, keepdims=True)
        # rescale grid to fit the camera dimensions
        squares /= np.amax(np.ptp(squares, axis=1) / dimensions)
        # save for later use
        square_path = square_path_proto.format(x_resolution)
        np.savez(square_path,
                 grid=squares,
                 resolution=(xres, yres))
