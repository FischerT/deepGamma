import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
from input_pipeline.h5_generators import NumpyReader
import tensorflow as tf
import numpy as np
from tensorflow.python import keras
import tensorflow as tf
import os
import sys
from time import time
from contextlib import ExitStack
from tensorflow.python.keras.regularizers import l2

DATA_ROOT = os.environ['MASTERDATA']

n_gpus = int(sys.argv[1]) or 1

with ExitStack() as stack:
    if n_gpus > 1:
        stack.enter_context(tf.device('/cpu:0'))
    model = keras.models.Sequential()
    model.add(keras.layers.Conv2D(32, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False, input_shape=(4, 32, 31)))
    model.add(keras.layers.Conv2D(32, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False))
    model.add(keras.layers.MaxPooling2D((3, 3), (2, 2), 'same', 'channels_first'))
    model.add(keras.layers.Conv2D(64, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False))
    model.add(keras.layers.Conv2D(64, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False))
    model.add(keras.layers.MaxPooling2D((3, 3), (2, 2), 'same', 'channels_first'))
    model.add(keras.layers.Conv2D(72, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False))
    model.add(keras.layers.Conv2D(72, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False))
    model.add(keras.layers.MaxPooling2D((3, 3), (2, 2), 'same', 'channels_first'))
    model.add(keras.layers.Conv2D(128, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False))
    model.add(keras.layers.Conv2D(128, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False))
    model.add(keras.layers.MaxPooling2D((3, 3), (2, 2), 'same', 'channels_first'))
    model.add(keras.layers.Conv2D(128, (1, 1), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False))
    model.add(keras.layers.Flatten())
    model.add(keras.layers.Dense(512, 'relu'))
    model.add(keras.layers.Dropout(0.5))
    model.add(keras.layers.Dense(512, 'relu'))
    model.add(keras.layers.Dropout(0.5))
    model.add(keras.layers.Dense(128, 'relu'))
    model.add(keras.layers.Dense(2, 'softmax'))

if n_gpus > 1:
    model = keras.utils.multi_gpu_model(model, n_gpus)

train_reader = NumpyReader(file_path=DATA_ROOT + 'classification/mcg_mcp/mcg_mcp_train.npz', requested_batches=np.inf,
                           batch_size=256*n_gpus, target='label')

valid_reader = NumpyReader(file_path=DATA_ROOT + 'classification/mcg_mcp/mcg_mcp_valid.npz', requested_batches=np.inf,
                           batch_size=4096*n_gpus, target='label')


print('Events in train: ', train_reader.total_events)
print('Events in valid: ', valid_reader.total_events)

model.compile(optimizer=keras.optimizers.Adam(lr=0.0006*n_gpus, decay=0.015),
              loss=keras.losses.sparse_categorical_crossentropy,
              metrics=['acc'])

print('valid loss:', model.evaluate_generator(generator = valid_reader.generator(), steps=valid_reader.total_batches))

model.fit_generator(generator=train_reader.generator(), steps_per_epoch=train_reader.total_batches//10,
                    epochs=20, verbose=1)

print('valid loss:', model.evaluate_generator(generator = valid_reader.generator(), steps=valid_reader.total_batches))
