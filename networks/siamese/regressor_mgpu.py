import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
from input_pipeline.h5_generators import NumpyReader
import tensorflow as tf
import numpy as np
from tensorflow.python import keras
import tensorflow as tf
import os
import sys
from time import time
from contextlib import ExitStack
from tensorflow.python.keras.regularizers import l2

DATA_ROOT = os.environ['MASTERDATA']

n_gpus = int(sys.argv[1]) or 1


def l2_loss(truth, prediction):
    residuals = truth - prediction
    loss = tf.norm(residuals, axis=1)
    return loss

def l1_loss(truth, prediction):
    residuals = truth - prediction
    loss = tf.norm(residuals, ord=1, axis=1)
    return loss


with ExitStack() as stack:
    if n_gpus > 1:
        stack.enter_context(tf.device('/cpu:0'))
    model = keras.models.Sequential()
    model.add(keras.layers.Conv2D(32, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False, kernel_regularizer=l2(0.005), input_shape=(4, 32, 31)))
    model.add(keras.layers.Conv2D(32, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False, kernel_regularizer=l2(0.005)))
    model.add(keras.layers.MaxPooling2D((3, 3), (2, 2), 'same', 'channels_first'))
    model.add(keras.layers.Conv2D(64, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False, kernel_regularizer=l2(0.005)))
    model.add(keras.layers.Conv2D(64, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False, kernel_regularizer=l2(0.005)))
    model.add(keras.layers.MaxPooling2D((3, 3), (2, 2), 'same', 'channels_first'))
    model.add(keras.layers.Conv2D(72, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False, kernel_regularizer=l2(0.005)))
    model.add(keras.layers.Conv2D(72, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False, kernel_regularizer=l2(0.005)))
    model.add(keras.layers.MaxPooling2D((3, 3), (2, 2), 'same', 'channels_first'))
    model.add(keras.layers.Conv2D(128, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False, kernel_regularizer=l2(0.005)))
    model.add(keras.layers.Conv2D(128, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False, kernel_regularizer=l2(0.001)))
    model.add(keras.layers.MaxPooling2D((3, 3), (2, 2), 'same', 'channels_first'))
    model.add(keras.layers.Conv2D(128, (1, 1), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False, kernel_regularizer=l2(0.005)))
    model.add(keras.layers.Flatten())
    model.add(keras.layers.Dense(512, 'relu'))
    model.add(keras.layers.Dropout(0.8))
    model.add(keras.layers.Dense(512, 'relu'))
    model.add(keras.layers.Dropout(0.8))
    model.add(keras.layers.Dense(128, 'tanh'))
    model.add(keras.layers.Dense(2))

if n_gpus > 1:
    model = keras.utils.multi_gpu_model(model, n_gpus)

train_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_train.npz', requested_batches=np.inf,
                           batch_size=256*n_gpus, target='nominal')

valid_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_valid.npz', requested_batches=np.inf,
                           batch_size=4096*n_gpus, target='nominal')


print('Events in train: ', train_reader.total_events)
print('Events in valid: ', valid_reader.total_events)

model.compile(optimizer=keras.optimizers.Adam(lr=0.01*n_gpus, decay=0.004),
              loss=l1_loss)

print('valid loss:', model.evaluate_generator(generator = valid_reader.generator(), steps=valid_reader.total_batches))

model.fit_generator(generator=train_reader.generator(), steps_per_epoch=train_reader.total_batches//5,
                    validation_data=valid_reader.generator(), validation_steps=valid_reader.total_batches,
                    epochs=10, verbose=1)

print('valid loss:', model.evaluate_generator(generator = valid_reader.generator(), steps=valid_reader.total_batches))

del train_reader
del valid_reader

eval_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_eval.npz', requested_batches=np.inf,
                           batch_size=4096*n_gpus, target='nominal')

predictions = model.predict_generator(eval_reader.generator(), steps=eval_reader.total_batches)
np.save('predictions', predictions)
