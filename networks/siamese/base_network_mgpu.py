import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
from input_pipeline.h5_generators import NumpyReader
import tensorflow as tf
import numpy as np
from tensorflow.python import keras
import tensorflow as tf
import os
import sys
from time import time

DATA_ROOT = os.environ['MASTERDATA']

n_gpus = int(sys.argv[1]) or 2

def l2_loss(truth, prediction):
    residuals = truth - prediction
    loss = tf.norm(residuals, axis=1)
    return loss

with tf.device('/cpu:0'):
    model = keras.models.Sequential()
    model.add(keras.layers.Conv2D(16, (3, 3), (1, 1), padding='same', data_format='channels_first', activation='relu', input_shape=(4, 32, 31)))
    model.add(keras.layers.Conv2D(32, (3, 3), (2, 2), padding='same', data_format='channels_first', activation='relu', input_shape=(4, 32, 31)))
    model.add(keras.layers.Conv2D(64, (3, 3), (2, 2), padding='same', data_format='channels_first', activation='relu'))
    model.add(keras.layers.Flatten())
    model.add(keras.layers.Dense(256, activation='relu'))
    model.add(keras.layers.Dropout(0.8))
    model.add(keras.layers.Dense(2))

model = keras.utils.multi_gpu_model(model, n_gpus)

train_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_train.npz', requested_batches=np.inf,
                           batch_size=4096*n_gpus, target='nominal')

valid_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_valid.npz', requested_batches=np.inf,
                           batch_size=4096*n_gpus, target='nominal')


print('Events in train: ', train_reader.total_events)
print('Events in valid: ', valid_reader.total_events)

model.compile(optimizer=keras.optimizers.Adam(lr=5, decay=0.01),
              loss=l2_loss,
              metrics=[])

model.fit_generator(generator=train_reader.generator(), steps_per_epoch=train_reader.total_batches//10,
                    epochs=1, verbose=1,
                    validation_data=valid_reader.generator(), validation_steps=valid_reader.total_batches)

del train_reader
del valid_reader

test_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_test.npz', requested_batches=0,
                           batch_size=4096*n_gpus, target='nominal')

print('test loss:', model.evaluate_generator(test_reader.generator(), steps=test_reader.total_batches))

del test_reader

eval_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_eval.npz', requested_batches=0,
                          batch_size=4096*n_gpus, target='nominal')

print('Events in eval: ', eval_reader.total_events)

start=time()
predictions = model.predict_generator(generator=eval_reader.generator(), steps=eval_reader.total_batches)
print('prediction took:', time()-start)
print('prediction shape:', predictions.shape)

np.save('predictions', predictions)
