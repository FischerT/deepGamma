from input_pipeline.dataset_building_frame2 import HESSDataset
from time import time
import os
DATA_ROOT = os.environ['MASTERDATA']

start = time()

gamma_path = DATA_ROOT + 'konrad_gamma_diffuse'
gammas = HESSDataset.from_path(gamma_path, 0, 100, 1)
gammas.shuffle()
train, valid, test = gammas.split(total_events=gammas.total_events)

train.store(DATA_ROOT + 'regression/konrad_gamma_train.h5')
valid.store(DATA_ROOT + 'regression/konrad_gamma_valid.h5')
test.store(DATA_ROOT + 'regression/konrad_gamma_test.h5')

print('Total time:', time() - start)
