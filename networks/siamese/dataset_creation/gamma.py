from input_pipeline.dataset_building_frame2 import HESSDataset
from time import time
import os
DATA_ROOT = os.environ['MASTERDATA']

start = time()

gamma_path = DATA_ROOT + 'gamma_diffuse_phase1'
gammas = HESSDataset.from_path(gamma_path, 0, 100, 2)
gammas.shuffle()
train, valid, test = gammas.split(total_events=gammas.total_events)

train.store_sparse(DATA_ROOT + 'regression/gamma_train')
valid.store_sparse(DATA_ROOT + 'regression/gamma_valid')
test.store_sparse(DATA_ROOT + 'regression/gamma_test')

print('Total time:', time() - start)
