from input_pipeline.dataset_building_frame2 import HESSDataset
from time import time
import os
DATA_ROOT = os.environ['MASTERDATA']

start = time()

proton_path = DATA_ROOT + 'gamma_diffuse_phase1'
gamma_path = DATA_ROOT + 'konrad_proton_diffuse_phase1'

protons = HESSDataset.from_path(proton_path, 0, 100, 2)
protons.shuffle()

gammas = HESSDataset.from_path(gamma_path, 0, 100, 2)
gammas.shuffle()

eventcount = min((protons.total_events, gammas.total_events))

p_train, p_valid, p_test = protons.split(total_events=eventcount)
g_train, g_valid, g_test = gammas.split(total_events=eventcount)

train = HESSDataset.interleave(p_train, g_train)
valid = HESSDataset.interleave(p_valid, g_valid)
test = HESSDataset.interleave(p_test, g_test)

print('storing')

train.store_sparse(DATA_ROOT + 'classification/mcg_mcp/mcg_mcp_train')
valid.store_sparse(DATA_ROOT + 'classification/mcg_mcp/mcg_mcp_valid')
test.store_sparse(DATA_ROOT + 'classification/mcg_mcp/mcg_mcp_test')

print('Total time:', time() - start)