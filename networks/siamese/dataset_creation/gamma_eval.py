from input_pipeline.dataset_building_frame2 import HESSDataset
from time import time
import os
DATA_ROOT = os.environ['MASTERDATA']

start = time()

gamma_path = DATA_ROOT + 'gamma_ps_phase1'
gammas = HESSDataset.from_path(gamma_path, 0, 100, 2)
gammas.shuffle()

gammas.store_sparse(DATA_ROOT + 'regression/gamma_eval')

print('Total time:', time() - start)
