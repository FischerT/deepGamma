import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
from input_pipeline.h5_generators import NumpyReader
import tensorflow as tf
import numpy as np
from tensorflow.python import keras
import os

DATA_ROOT = os.environ['MASTERDATA']


GPU_AVAIL = tf.test.is_gpu_available()
if GPU_AVAIL:
    DATA_FORMAT = 'channels_first'
else:
    DATA_FORMAT = 'channels_last'


def l2_loss(truth, prediction):
    residuals = truth - prediction
    loss = tf.norm(residuals, axis=1)
    return loss

model = keras.models.Sequential()

if not GPU_AVAIL:
    model.add(keras.layers.Permute((2, 3, 1), input_shape=(4, 32, 31)))
model.add(keras.layers.Conv2D(16, (3, 3), (2, 2), padding='same', data_format=DATA_FORMAT, activation='relu', input_shape=(4, 32, 31)))
model.add(keras.layers.Conv2D(32, (3, 3), (2, 2), padding='same', data_format=DATA_FORMAT, activation='relu'))
model.add(keras.layers.Flatten())
model.add(keras.layers.Dense(100, activation='relu'))
model.add(keras.layers.Dense(2))

train_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_train.npz', requested_batches=np.inf,
                           batch_size = 8192, target='nominal')

valid_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_valid.npz', requested_batches=np.inf,
                           batch_size = 8192, target='nominal')

model.compile(optimizer=keras.optimizers.Adam(lr=5, decay=0.01),
              loss=l2_loss,
              metrics=[])

model.fit_generator(generator=train_reader.generator(), steps_per_epoch=train_reader.total_batches*5,
                    epochs=500, verbose=2,
                    validation_data=valid_reader.generator(), validation_steps=valid_reader.total_batches*5)

