import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
from input_pipeline.h5_generators import NumpyReader
import tensorflow as tf
import numpy as np
from tensorflow.python import keras
import tensorflow as tf
import os
import sys
from time import time
from contextlib import ExitStack
from tensorflow.python.keras.regularizers import l2

DATA_ROOT = os.environ['MASTERDATA']

n_gpus = int(sys.argv[1]) or 1

if len(sys.argv)==3:
    model=keras.models.load_model(sys.argv[2])
else:
    with ExitStack() as stack:
        if n_gpus > 1:
            stack.enter_context(tf.device('/cpu:0'))
        inputs = keras.layers.Input(shape=(4, 32, 31))

        twin = keras.models.Sequential()
        twin.add(keras.layers.Conv2D(16, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False,
                                  input_shape=(1, 32, 31)))
        twin.add(keras.layers.Conv2D(16, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False))
        twin.add(keras.layers.MaxPooling2D((3, 3), (2, 2), 'same', 'channels_first'))
        twin.add(keras.layers.Conv2D(32, (3, 3), (1, 1), 'same', 'channels_first', activation='relu', use_bias=False))
        twin.add(keras.layers.Conv2D(32, (3, 3), (2, 2), 'same', 'channels_first', activation='relu', use_bias=False))
        twin.add(keras.layers.MaxPooling2D((3, 3), (2, 2), 'same', 'channels_first'))
        twin.add(keras.layers.Flatten())
        twin.add(keras.layers.Dense(128, 'relu'))

        twin1 = twin(keras.layers.Lambda(lambda inputs: inputs[:, 0:1], output_shape=(None, 1, 32, 31))(inputs))
        twin2 = twin(keras.layers.Lambda(lambda inputs: inputs[:, 1:2], output_shape=(None, 1, 32, 31))(inputs))
        twin3 = twin(keras.layers.Lambda(lambda inputs: inputs[:, 2:3], output_shape=(None, 1, 32, 31))(inputs))
        twin4 = twin(keras.layers.Lambda(lambda inputs: inputs[:, 3:4], output_shape=(None, 1, 32, 31))(inputs))

        merge = keras.layers.concatenate([twin1, twin2, twin3, twin4])

        x = keras.layers.Dense(512, activation='relu')(merge)
        x = keras.layers.Dropout(0.8)(x)
        x = keras.layers.Dense(256, activation='relu')(x)
        x = keras.layers.Dropout(0.8)(x)
        predictions = keras.layers.Dense(2, activation='softmax')(x)

        model = keras.models.Model(inputs=inputs, outputs=predictions)


if n_gpus > 1:
    model = keras.utils.multi_gpu_model(model, n_gpus)

#train_reader = NumpyReader(file_path=DATA_ROOT + 'classification/real_real/34855_034881_train.npz', requested_batches=np.inf,
train_reader = NumpyReader(file_path=DATA_ROOT + 'classification/real_mcp/34855_mcp_train.npz', requested_batches=np.inf,
#train_reader = NumpyReader(file_path=DATA_ROOT + 'classification/mcg_mcp/mcg_mcp_train.npz', requested_batches=np.inf,
                           batch_size=256*n_gpus, target='label')

#valid_reader = NumpyReader(file_path=DATA_ROOT + 'classification/real_real/34855_034881_valid.npz', requested_batches=np.inf,
valid_reader = NumpyReader(file_path=DATA_ROOT + 'classification/real_mcp/34855_mcp_valid.npz', requested_batches=np.inf,
#valid_reader = NumpyReader(file_path=DATA_ROOT + 'classification/mcg_mcp/mcg_mcp_valid.npz', requested_batches=np.inf,
                           batch_size=4096*n_gpus, target='label')

print('Events in train: ', train_reader.total_events)
print('Events in valid: ', valid_reader.total_events)

model.compile(optimizer=keras.optimizers.Adam(lr=0.004*n_gpus, decay=0.001),
              loss=keras.losses.sparse_categorical_crossentropy,
              metrics=['acc'])

print('valid loss:', model.evaluate_generator(generator = valid_reader.generator(), steps=valid_reader.total_batches))

model.fit_generator(generator=train_reader.generator(), steps_per_epoch=train_reader.total_batches,
                    epochs=50, verbose=1,
#                    validation_data=valid_reader.generator(), validation_steps=valid_reader.total_batches
                    )

model.save('mymodel.h5')

print('valid loss:', model.evaluate_generator(generator = valid_reader.generator(), steps=valid_reader.total_batches))

del train_reader
del valid_reader

#test_reader = NumpyReader(file_path=DATA_ROOT + 'classification/real_real/34855_034881_test.npz', requested_batches=np.inf,
test_reader = NumpyReader(file_path=DATA_ROOT + 'classification/real_mcp/34855_mcp_test.npz', requested_batches=np.inf,
                           batch_size=4096*n_gpus, target='label')


predictions = model.predict_generator(test_reader.generator(), test_reader.total_batches)
np.save('predictions_test.npy', predictions[:, 0])

test_reader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_test.npz', requested_batches=np.inf,
                           batch_size=4096*n_gpus, target='label')

predictions = model.predict_generator(test_reader.generator(), test_reader.total_batches)
np.save('predictions_gamma.npy', predictions[:, 0])
