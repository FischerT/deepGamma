import tensorflow as tf

import input_pipeline.h5_generators as h5g
import input_pipeline.image_preprocessing

from tqdm import tqdm
from time import time


file = '/home/obay/Dropbox/masterThesis/data/gamma_diffuse.lz4_128.h5'

ds = h5g.SequentialH5Reader(file, 128).get_tf_dataset()
value = ds.make_one_shot_iterator().get_next()

with tf.Session() as sess:
    start = time()
    for _ in tqdm(range(600)):
        sess.run(value)
    time_per_batch = (time() - start)/600
    print(time_per_batch)
