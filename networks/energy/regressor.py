import tensorflow as tf
import numpy as np

from input_pipeline.h5_generators import SequentialH5Reader
from utilities.display_dataset_properties import DatasetDisplay

from tqdm import tqdm

def log10(x):
  numerator = tf.log(x)
  denominator = tf.log(tf.constant(10, dtype=numerator.dtype))
  return numerator / denominator

gamma_file = '/home/obay/Dropbox/masterThesis/data/gamma_diffuse.lz4_128.h5'
gamma_ds = SequentialH5Reader(gamma_file, chunk_size=128).get_tf_dataset()
gamma_batch = gamma_ds.make_one_shot_iterator().get_next()

disp = DatasetDisplay(gamma_file, "dummy")
disp.energy_histo()
disp.pp.close()

losses = []


image_batch = gamma_batch['cam1_Images']

conv1 = tf.layers.conv2d(inputs=image_batch,
                         filters=32,
                         kernel_size=3,
                         strides=2,
                         padding='same',
                         data_format='channels_first',
                         activation=tf.nn.relu,
                         )

print(conv1.get_shape().as_list())

conv2 = tf.layers.conv2d(inputs=conv1,
                         filters=64,
                         kernel_size=3,
                         strides=2,
                         padding='same',
                         data_format='channels_first',
                         activation=tf.nn.relu)

print(conv2.get_shape().as_list())

conv3 = tf.layers.conv2d(inputs=conv2,
                         filters=128,
                         kernel_size=3,
                         strides=2,
                         padding='same',
                         data_format='channels_first',
                         activation=tf.nn.relu)

print(conv3.get_shape().as_list())

conv4 = tf.layers.conv2d(inputs=conv3,
                         filters=256,
                         kernel_size=3,
                         strides=2,
                         padding='same',
                         data_format='channels_first',
                         activation=tf.nn.relu)

print(conv4.get_shape().as_list())

conv2_flat = tf.reshape(conv4, [-1, 256*4*3])

dense = tf.layers.dense(inputs=conv2_flat, units=1024, activation=tf.nn.relu)

dropout = tf.layers.dropout(inputs=dense, rate=0.4, training=True)

dense2 = tf.layers.dense(inputs=dropout, units=1024, activation=tf.nn.relu)

dropout2 = tf.layers.dropout(inputs=dense2, rate=0.4, training=True)

dense3 = tf.layers.dense(inputs=dropout2, units=1, activation=tf.nn.relu)

output = tf.reshape(dense3, [-1])

loss = tf.losses.absolute_difference(output, log10(gamma_batch['Energy']))

optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.0001)

train_op = optimizer.minimize(loss)

with tf.Session() as sess:
    tf.global_variables_initializer().run()

    for x in range(500):
        losses.append(sess.run([loss, train_op])[0])


