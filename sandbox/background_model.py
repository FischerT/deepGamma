import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
from astropy import units as u
from astropy.coordinates import EarthLocation, SkyCoord, ICRS
from astropy.time import Time
import scipy.ndimage as sp
from astropy.convolution import convolve
from astropy.convolution.kernels import Gaussian2DKernel, Tophat2DKernel

import pickle

#df = pd.read_csv('/home/obay/Dropbox/masterThesis/az_test_df.csv')
#al_df = pd.read_csv('/home/obay/Dropbox/masterThesis/alt_test_df.csv')
#
#df.sort_values(by='event_id')
#al_df.sort_values(by='event_id')
#
#df['predicted_dalt'] = al_df['predicted_dalt']
#
#event_time = df['event_time'].values
#event_time = np.float64(event_time)
#
##  HESS Lon = 16.5028 Lat = -23.27280 elevation = 1800 m
hess_location = EarthLocation(lat=-23.27280 * u.deg, lon=16.5028 * u.deg, height=1800 * u.m)
#observation_time = Time(event_time, format='unix')
#
#pos_alt = df['pos_alt'].values
#pos_az = df['pos_az'].values
#
#pos_altaz_skycoord = SkyCoord(alt=pos_alt * u.deg, az=pos_az * u.deg, frame='altaz',
#                              obstime=observation_time, location=hess_location,
#                              pressure=1013 * u.hPa)
#
#pos_radec = pos_altaz_skycoord.transform_to(ICRS)
#pos_ra = pos_radec.ra.degree
#pos_dec = pos_radec.dec.degree
#
#pred_dalt = df['predicted_dalt'].values
#pred_daz = df['predicted_daz'].values
#
#pred_tar_alt = pos_alt + pred_dalt
#pred_tar_az = pos_az + pred_daz
#
#pred_tar_altaz = SkyCoord(alt=pred_tar_alt * u.deg, az=pred_tar_az * u.deg, frame='altaz',
#                          obstime=observation_time, location=hess_location,
#                          pressure=1013 * u.hPa)
#pred_tar_radec = pred_tar_altaz.transform_to(ICRS)
#pickle.dump(pred_tar_radec, open('pred_dar_radec.p', 'wb'))
#pickle.dump(observation_time, open('observation_time.p', 'wb'))
#pickle.dump(pos_radec, open('pos_radec.p', 'wb'))


pred_tar_radec = pickle.load(open('pred_dar_radec.p', 'rb'))
observation_time = pickle.load(open('observation_time.p', 'rb'))
pos_radec = pickle.load(open('pos_radec.p', 'rb'))


true_tar_radec = SkyCoord(329.7208 * u.deg, -30.2217 * u.deg, frame=ICRS,
                          obstime=observation_time, location=hess_location,
                          pressure=1013 * u.hPa)


@np.vectorize
def source_counts(source_extent, ring_extent):
    if source_extent > ring_extent: return 0

    r_inner = 0.5 - ring_extent
    r_outer = 0.5 + ring_extent

    on_ring_outside_source_pred_tar_radec_bidx = (r_inner < pos_radec.separation(pred_tar_radec).degree) & \
                                                 (r_outer > pos_radec.separation(pred_tar_radec).degree) & \
                                                 (source_extent < pred_tar_radec.separation(true_tar_radec).degree)

    background_radec = pred_tar_radec[on_ring_outside_source_pred_tar_radec_bidx]
    source_radec = pred_tar_radec[source_extent > pred_tar_radec.separation(true_tar_radec).degree]

    background_area = np.pi*(r_outer**2 - r_inner**2 - source_extent**2)
    background_counts = np.sum(on_ring_outside_source_pred_tar_radec_bidx)
    background_density = background_counts/background_area

    source_area = np.pi*source_extent**2
    background = source_area*background_density

    hist, xedges, yedges = np.histogram2d(source_radec.ra.degree, source_radec.dec.degree, bins=600,
                          range=np.array([(329.7208 - 3, 329.7208 + 3), (-30.2217 - 3, -30.2217 + 3)]))

    return np.sum(hist)-background

ring_extent = 0.5
source_extent = 0.3

# source_extent, ring_extent = np.meshgrid(np.linspace(0.01, 0.5, 200), np.linspace(0.01, 0.5, 200))
#
# counts = source_counts(source_extent, ring_extent)
#
# plt.pcolormesh(source_extent, ring_extent, counts)
# plt.axis('equal')
# plt.xlabel('source extent')
# plt.ylabel('ring extent')
# plt.colorbar()
# plt.show()

#plt.hist2d(pred_tar_radec.ra.degree, pred_tar_radec.dec.degree, bins=600,
#           range=np.array([(329.7208 - 3, 329.7208 + 3), (-30.2217 - 3, -30.2217 + 3)]))
#plt.title('PKS-2155 Count map')
#plt.xlabel('R.A. [deg]')
#plt.ylabel('Dec. [deg]')
#plt.axis('equal')
#cb = plt.colorbar()
#cb.set_label('Counts')
#plt.show()
#
#plt.hist2d(background_radec.ra.degree, background_radec.dec.degree, bins=600,
#           range=np.array([(329.7208 - 3, 329.7208 + 3), (-30.2217 - 3, -30.2217 + 3)]))
#plt.title('PKS-2155 Count map BACKGROUND')
#plt.xlabel('R.A. [deg]')
#plt.ylabel('Dec. [deg]')
#plt.axis('equal')
#cb = plt.colorbar()
#cb.set_label('Counts')
#plt.show()


r_inner = 0.5 - ring_extent
r_outer = 0.5 + ring_extent

on_ring_outside_source_pred_tar_radec_bidx = (r_inner < pos_radec.separation(pred_tar_radec).degree) & \
                                             (r_outer > pos_radec.separation(pred_tar_radec).degree) & \
                                             (source_extent < pred_tar_radec.separation(true_tar_radec).degree)

background_radec = pred_tar_radec[on_ring_outside_source_pred_tar_radec_bidx]
source_radec = pred_tar_radec[source_extent > pred_tar_radec.separation(true_tar_radec).degree]

background_area = np.pi*(r_outer**2 - r_inner**2 - source_extent**2)
background_counts = np.sum(on_ring_outside_source_pred_tar_radec_bidx)
background_density = background_counts/background_area

source_area = np.pi*source_extent**2
background = source_area*background_density

hist, xedges, yedges = np.histogram2d(source_radec.ra.degree, source_radec.dec.degree, bins=600,
                      range=np.array([(329.7208 - 3, 329.7208 + 3), (-30.2217 - 3, -30.2217 + 3)]))

plt.imshow(hist.T, origin='low', extent=[xedges[0], xedges[-1],yedges[0],yedges[-1]])
plt.title('PKS-2155 Count map')
plt.xlabel('R.A. [deg]')
plt.ylabel('Dec. [deg]')
plt.axis('equal')
cb = plt.colorbar()
cb.set_label('Counts')
plt.show()


