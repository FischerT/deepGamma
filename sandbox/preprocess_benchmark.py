import tensorflow as tf
import numpy as np

import input_pipeline.h5_generators as h5g
import input_pipeline.image_preprocessing as pp

from tqdm import tqdm
from time import time


# generate tensorflow matrices
matrix, resolution = pp._get_rebin_matrix_and_resolution(32, True)
coo_matrix = matrix.tocoo()
dense_matrix = coo_matrix.todense()
indices = np.vstack((coo_matrix.row, coo_matrix.col)).T
data = coo_matrix.data.astype(np.float32)
dense_shape = coo_matrix.shape

with tf.device('/cpu:0'):
    tf_sp_pp_matrix = tf.SparseTensor(indices, data, dense_shape)
    tf_dense_pp_matrix = tf.constant(dense_matrix, dtype=tf.float32)


    file = '/home/obay/Dropbox/masterThesis/data/nn_dsts.lz4_128.h5'
    ds = h5g.SequentialH5Reader(file, 128, preprocessing=None).get_tf_dataset().repeat()
    initial_value = ds.make_one_shot_iterator().get_next()['cam1_Images']
    value = tf.reshape(initial_value, shape=[-1, 960], name="flatten_batch")
    value = tf.transpose(value, name="transpose_flat_batch")

    preprocessed_sparse = tf.sparse_tensor_dense_matmul(tf_sp_pp_matrix, value, name="sparse_preproc")
    preprocessed_sparse = tf.reshape(tf.transpose(preprocessed_sparse, name="sparse_untranspose"), shape=[128, 4, *resolution], name="sparse_unreshape")

    preprocessed_dense = tf.matmul(tf_dense_pp_matrix, value, name="dense_preproc")
    preprocessed_dense = tf.reshape(tf.transpose(preprocessed_dense, name="dense_untranspose"), shape=[128, 4, *resolution], name="dense_unreshape")

    ds_preproc = h5g.SequentialH5Reader(file, 128, rebin_resolution=32).get_tf_dataset()
    initial_value_preproc = ds_preproc.make_one_shot_iterator().get_next()['cam1_Images']

    ds_preproc_single = h5g.SequentialH5Reader(file, 128, rebin_resolution=32, yield_chunk=False).get_tf_dataset().batch(128)
    initial_value_preproc_single = ds_preproc_single.make_one_shot_iterator().get_next()['cam1_Images']

with tf.device('/gpu:0'):
    preprocessed_sparse_gpu = tf.sparse_tensor_dense_matmul(tf_sp_pp_matrix, value, name="sparse_preproc")
    preprocessed_sparse_gpu = tf.reshape(tf.transpose(preprocessed_sparse, name="sparse_untranspose"),
                                     shape=[128, 4, *resolution], name="sparse_unreshape")

    preprocessed_dense_gpu = tf.matmul(tf_dense_pp_matrix, value, name="dense_preproc")
    preprocessed_dense_gpu = tf.reshape(tf.transpose(preprocessed_dense, name="dense_untranspose"),
                                    shape=[128, 4, *resolution], name="dense_unreshape")


with tf.Session(config=tf.ConfigProto(log_device_placement=True)) as sess:
    num_batches = 600

    start = time()
    for _ in tqdm(range(num_batches), desc='scipy_sparse_single'):
        sess.run(initial_value_preproc_single)
    time_per_batch = (time() - start) / 600
    print('scipy_sparse single', time_per_batch)

    start = time()
    for _ in tqdm(range(num_batches), desc='scipy_sparse'):
        sess.run(initial_value_preproc)
    time_per_batch = (time() - start) / 600
    print('scipy_sparse', time_per_batch)

    start = time()
    for _ in tqdm(range(num_batches), desc='raw'):
        sess.run(value)
    time_per_batch = (time() - start)/600
    print('raw', time_per_batch)

    start = time()
    for _ in tqdm(range(num_batches), desc='tf_sparse_gpu'):
        sess.run(preprocessed_sparse_gpu)
    time_per_batch = (time() - start) / 600
    print('tf_sparse_gpu', time_per_batch)

    start = time()
    for _ in tqdm(range(num_batches), desc='tf_dense_gpu'):
        sess.run(preprocessed_dense_gpu)
    time_per_batch = (time() - start) / 600
    print('tf_dense_gpu', time_per_batch)

    start = time()
    for _ in tqdm(range(num_batches), desc='tf_sparse'):
        sess.run(preprocessed_sparse)
    time_per_batch = (time() - start) / 600
    print('tf_sparse', time_per_batch)

    start = time()
    for _ in tqdm(range(num_batches), desc='tf_dense'):
        sess.run(preprocessed_dense)
    time_per_batch = (time() - start) / 600
    print('tf_dense', time_per_batch)




