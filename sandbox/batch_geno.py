# better implementation: start with counter, don't use range loop.
def geno():
    start_batch = 72
    requested_batches = 120

    batch_size = 3
    meta_size = 2
    total_elements = 291

    meta_size *= batch_size # elements in metachunk instead of batches in metachunk
    total_elements -= total_elements % batch_size
    total_batches = total_elements // batch_size
    print('total batches:', total_batches)

    # lower and upper indices for metachunks
    lower_batch_idx = start_batch
    while True:
        # wraparound at end of dataset
        lower_batch_idx %= total_batches

        # calculate upper index for full metachunk
        upper_batch_idx = lower_batch_idx + meta_size
        # truncate metachunk if at end of dataset
        upper_batch_idx = min(upper_batch_idx, total_batches)

        batches_in_chunk = upper_batch_idx - lower_batch_idx
        requested_batches -= batches_in_chunk

        # yield smaller last metachunk to not overshoot requested batches
        # this is also the end of the generator
        if requested_batches <= 0:
            upper_batch_idx += requested_batches
            yield (lower_batch_idx, upper_batch_idx)
            break

        yield (lower_batch_idx, upper_batch_idx)
        lower_batch_idx += batches_in_chunk

for x in geno():
    print(x)