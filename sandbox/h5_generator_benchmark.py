import tensorflow as tf
import numpy as np
import input_pipeline.h5_generators as h5g
from tqdm import tqdm
from itertools import islice
import multiprocessing as mp
import tensorflow as tf
from time import time
import os
DATA_ROOT = os.environ['MASTERDATA']

NUM_BENCH_EVENTS = 1000
bench_file_path = '/home/obay/Dropbox/masterThesis/data/new_exports/classification/sim_real/train.h5'
reader = h5g.SequentialH5Reader(bench_file_path, steps=1000*128, rebin_resolution=32)

# Dummy data
start = time()
for _ in islice(reader._fast_dummy_generator(), 0, NUM_BENCH_EVENTS):
    pass
print('time per dummy batch ', (time()-start)/NUM_BENCH_EVENTS)
[child.terminate() for child in mp.active_children()]

ds = reader._get_fast_dummy_tf_dataset()
item = ds.make_one_shot_iterator().get_next()
with tf.Session() as sess:
    sess.run(item)
    start = time()
    for _ in range(500):
        sess.run(item)
print('time per dummy batch ', (time()-start)/NUM_BENCH_EVENTS)
[child.terminate() for child in mp.active_children()]


# Actual data
start = time()
for _ in islice(reader.generator(), 0, NUM_BENCH_EVENTS):
    pass
print('time per batch ', (time()-start)/NUM_BENCH_EVENTS)
[child.terminate() for child in mp.active_children()]

ds = reader.get_tf_dataset()
item = ds.make_one_shot_iterator().get_next()
with tf.Session() as sess:
    sess.run(item)
    start = time()
    for _ in range(NUM_BENCH_EVENTS):
        sess.run(item)
print('time per batch ', (time() - start) / NUM_BENCH_EVENTS)
[child.terminate() for child in mp.active_children()]