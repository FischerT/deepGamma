import numpy as np


def image_batch_generator(lengths, idxs, vals, shape, batch_size):
    # get some sparse data and yield single dense images
    idxs %= np.prod((batch_size, *shape[1:]))
    lengths = np.sum(lengths.reshape(-1, batch_size), axis=1)

    lower_idx = 0
    for length in lengths:
        upper_idx = lower_idx + length
        data = np.zeros((batch_size, *shape[1:]))
        data.ravel()[idxs[lower_idx:upper_idx]] = vals[lower_idx:upper_idx]
        lower_idx = upper_idx
        yield data


def image_generator(lengths, idxs, vals, shape):
    # get some sparse data and yield single dense images
    idxs %= np.prod(shape[1:])

    lower_idx = 0
    for length in lengths:
        upper_idx = lower_idx + length
        data = np.zeros(shape[1:])
        data.ravel()[idxs[lower_idx:upper_idx]] = vals[lower_idx:upper_idx]
        lower_idx = upper_idx
        yield data


def disassemble_data(data):
    lengths = np.count_nonzero(data, axis=(1, ))#2, 3))
    idxs = np.flatnonzero(data)
    vals = data.ravel()[idxs]

    return lengths, idxs, vals, data.shape


def assemble_data(lengths, idxs, vals, shape):
    data = np.zeros(shape)

    lower_idx = 0
    for length in lengths:
        upper_idx = lower_idx + length
        data.ravel()[idxs[lower_idx:upper_idx]] = vals[lower_idx:upper_idx]
        lower_idx = upper_idx
    return data


#my_data = np.random.uniform(0, 1, (6, 3, ))#2, 2))
#my_data[my_data > 0.5] = 0
my_data = np.arange(6*3).reshape(6, 3)


sparse_data = disassemble_data(my_data)
my_new_data = assemble_data(*sparse_data)

assert np.allclose(my_data, my_new_data)

print(my_data)

print('=====')

for element in image_generator(*sparse_data):
    print(element)

print('=====')

for element in image_batch_generator(*sparse_data, 2):
    print(element)
