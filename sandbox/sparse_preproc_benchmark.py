from input_pipeline.image_preprocessing import _get_rebin_matrix_and_resolution
import matplotlib.pyplot as plt
import scipy.sparse as sp
import numpy as np
from time import time
from utilities.display_events import cam1_hex_event_figure, cam1_square_event_figure

# reshape [batch_size, channels, image] to [image] and back
batch_size = 1
channels = 4
image_size = 960
dummy = np.zeros((batch_size, channels, image_size), dtype=np.float32)
for batch in range(batch_size):
    for channel in range(channels):
        dummy[batch, channel] = np.hstack((np.arange(channel*10), np.zeros(960-channel*10)))

matrix, resolution = _get_rebin_matrix_and_resolution(50, True)

result = matrix.dot(dummy.reshape(-1, 960).T).T.reshape(1, 4, resolution[0], resolution[1]).transpose(0, 1, 3, 2)

cam1_hex_event_figure(dummy[0], True)
cam1_square_event_figure(np.squeeze(result))

plt.show()