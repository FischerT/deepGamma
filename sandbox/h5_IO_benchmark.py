import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import numpy as np
from time import time, sleep
import sys
from tqdm import tqdm
import matplotlib.pyplot as plt
import os
import multiprocessing as mp
from itertools import repeat


data_format = {
    'id': [np.int64, [5]],
    'label': [np.int64, []],
    'cam1_images': [np.float32, [4, 960]],
    'cam2_images': [np.float32, [1, 2048]],
    'cam1_size': [np.float32, [4]],
    'cam2_size': [np.float32, [1]],
    'cam1_width': [np.float32, [4]],
    'cam2_width': [np.float32, [1]],
    'cam1_length': [np.float32, [4]],
    'cam2_length': [np.float32, [1]],
    'cam1_loc_dis': [np.float32, [4]],
    'cam2_loc_dis': [np.float32, [1]],
    'cam1_phi': [np.float32, [4]],
    'cam2_phi': [np.float32, [1]],
    'cam1_cog_x': [np.float32, [4]],
    'cam2_cog_x': [np.float32, [1]],
    'cam1_cog_y': [np.float32, [4]],
    'cam2_cog_y': [np.float32, [1]],
    'cam1_skewness': [np.float32, [4]],
    'cam2_skewness': [np.float32, [1]],
    'cam1_kurtosis': [np.float32, [4]],
    'cam2_kurtosis': [np.float32, [1]],
    'alt_az': [np.float32, [2]],
    'ra_dec': [np.float32, [2]],
    'core_ground': [np.float32, [3]],
    'core_tilted': [np.float32, [3]],
    'energy': [np.float32, []],
    'time': [np.float64, []]
}


def repack_file_to_lz4(file_path, chunksize):
    data = {}
    data_shape = {}
    data_maxshape = {}
    data_dtype = {}
    data_chunks = {}

    with h5py.File(file_path, 'r') as f:
        for name, value in data_format.items():
            data[name] = f[name].value
            data_shape[name] = f[name].shape
            data_maxshape[name] = f[name].maxshape
            data_dtype[name] = f[name].dtype
            data_chunks[name] = (chunksize,) + f[name].chunks[1:]

    with h5py.File(file_path, 'w') as f:
        for name, value in data_format.items():
            f.create_dataset(name, shape=data_shape[name], maxshape=data_maxshape[name], dtype=data_dtype[name],
                             chunks=data_chunks[name], compression=32004, shuffle=True, data=data[name])


def repack_folder_to_lz4(folder_path, chunksize):
    file_paths = [os.path.join(folder_path, f) for f in os.listdir(folder_path)
                  if os.path.join(folder_path, f).endswith('.h5') and os.path.isfile(os.path.join(folder_path, f))]
    with mp.Pool(os.cpu_count(), maxtasksperchild=1) as p:
        p.starmap(repack_file_to_lz4, zip(file_paths, repeat(chunksize)))


def calculate_slices(total_events, chunk_size, yield_last=False):
    # generate list of slices to iterate over
    # e.g. for total_events = 26 and chunk_size = 10:
    # [[0:10], [10:20], [20:26]]

    if chunk_size > total_events:
        return [(0, total_events)]

    slices = []
    for index in range(chunk_size, total_events+1, chunk_size):
        slices.append((index-chunk_size, index))

    if yield_last and total_events % chunk_size:
        slices.append((index, index + (total_events % chunk_size)))

    return slices


def chunk_read(chunk_size):
    slices = calculate_slices(120000, chunk_size, yield_last=True)
    with h5py.File('/home/obay/Dropbox/masterThesis/data/new_exports/classification/valid.h5', 'r') as f:
        for key in data_format.keys():
            for slc in slices:
                data = f[key][slice(*slc)]


def chunk_read_direct(chunk_size):
    slices = calculate_slices(120000, chunk_size, yield_last=True)
    with h5py.File('/home/obay/Dropbox/masterThesis/data/new_exports/classification/valid.h5', 'r') as f:
        for name, props in data_format.items():
            for slc in slices:
                data = np.empty([slc[1]-slc[0]] + props[1], props[0])
                f[name].read_direct(data, np.s_[slc[0]:slc[1]], np.s_[0: slc[1]-slc[0]])
                # data2 = f[name][np.s_[slc[0]:slc[1]]]
                # print(slc, np.allclose(data, data2))


def benchmark_chunked_read():

    all_times = []
    all_times_direct = []
    powers = np.arange(7, 17, .3)
    chunk_sizes = [int(2**x) for x in powers]
    for chunk_size in tqdm(chunk_sizes):
        times = []
        times_direct = []
        for _ in range(3):
            start = time()
            chunk_read_direct(chunk_size)
            times_direct.append(time()-start)

            start = time()
            chunk_read(chunk_size)
            times.append(time()-start)

        all_times.append(times)
        all_times_direct.append(times_direct)
    time_mean = [np.mean(time) for time in all_times]
    time_var = [np.std(time) for time in all_times]

    time_mean_direct = [np.mean(time) for time in all_times_direct]
    time_var_direct = [np.std(time) for time in all_times_direct]

    plt.errorbar(chunk_sizes, time_mean, time_var, c='r')
    plt.errorbar(chunk_sizes, time_mean_direct, time_var_direct, c='b')
    plt.gca().set_xscale('log')
    plt.gca().set_yscale('log')
    plt.show()


def dict_size(dict):
    element_sizes = [sys.getsizeof(element) for element in dict.values()]
    return sum(element_sizes)


def get_h5_as_dict(file_path):
    with h5py.File(file_path, 'r') as f:
        data = {
            'IDs': f['IDs'][:],
            #'cam1_Images': f['cam1_Images'][:],
            #'cam2_Images': f['cam2_Images'][:],
            'AltAz': f['AltAz'][:],
            'RaDec': f['RaDec'][:],
            'Energy': f['Energy'][:],
            'Size': f['Size'][:],
            'Time': f['Time'][:]
        }
    return data


def write_dict_to_h5(data, file_path, chunksize, compression_level=0):
    with h5py.File(file_path, 'w') as f:
        for name, value in data.items():
            if name in ['cam1_Images', 'cam2_Images']:
                chunks = (chunksize,) + tuple(value.shape[1:])
            else:
                chunks = value.shape

            if compression_level == 'lz4':
                dataset = f.create_dataset(name=name, shape=value.shape,
                                           chunks=chunks, dtype=value.dtype,
                                           compression=32004, shuffle=False)
            elif compression_level == 'lz4s':
                dataset = f.create_dataset(name=name, shape=value.shape,
                                           chunks=chunks, dtype=value.dtype,
                                           compression=32004, shuffle=True)

            elif compression_level == 'lzf':
                dataset = f.create_dataset(name=name, shape=value.shape,
                                           chunks=chunks, dtype=value.dtype,
                                           compression='lzf')
            elif compression_level in range(10):
                dataset = f.create_dataset(name=name, shape=value.shape,
                                           chunks=chunks, dtype=value.dtype,
                                           compression='gzip', compression_opts=compression_level)
            else:
                raise ValueError('Compression option wrong: ', compression_level)
            dataset[:] = value


def benchmark_rw():
    """
    Loads whole file into ram, writes contents to new file
    """
    input_file = '/home/obay/Dropbox/masterThesis/data/single_files/proton_diffuse.lz4_128.h5'
    dummy_file = '/home/obay/Dropbox/masterThesis/data/DUMMY.h5'
    start = time()
    data = get_h5_as_dict(input_file)
    print('Reading took {}s'.format(time() - start))
    print('Size of data: ', dict_size(data) / (1024 ** 2), 'Mb')
    write_times = {}
    write_time_errors = {}
    read_times = {}
    read_time_errors = {}
    # chunksizes = range(1, 200, 5)
    chunksizes = [2 ** x for x in range(18)]
    comp_lvls = ['lz4', 'lz4s', 'lzf', 0, 5, 9]
    colors = 'rgbcmy'
    for comp_lvl in comp_lvls:
        write_times[comp_lvl] = []
        write_time_errors[comp_lvl] = []
        read_times[comp_lvl] = []
        read_time_errors[comp_lvl] = []
        for chunksize in tqdm(chunksizes, desc=str(comp_lvl)):
            repeated_write_times = np.empty(10)
            repeated_read_times = np.empty(10)
            for repeat in range(10):
                start = time()
                write_dict_to_h5(data, dummy_file, chunksize)
                repeated_write_times[repeat] = time() - start

                data = {}

                start = time()
                data = get_h5_as_dict(dummy_file)
                repeated_read_times[repeat] = time() - start

            write_times[comp_lvl].append(np.mean(repeated_write_times))
            write_time_errors[comp_lvl].append(np.std(repeated_write_times))

            read_times[comp_lvl].append(np.mean(repeated_read_times))
            read_time_errors[comp_lvl].append(np.std(repeated_read_times))
    for comp_lvl, color in zip(comp_lvls, colors):
        plt.errorbar(x=chunksizes, y=write_times[comp_lvl], yerr=write_time_errors[comp_lvl],
                     label='Write %s' % comp_lvl, color=color, ls='-')
        plt.errorbar(x=chunksizes, y=read_times[comp_lvl], yerr=read_time_errors[comp_lvl], label='Read %s' % comp_lvl,
                     color=color, ls='--')
    plt.xscale('log')
    plt.legend()
    plt.show()


def benchmark_random_write():
    input_file = '/home/obay/Dropbox/masterThesis/data/single_files/proton_diffuse.lz4_128.h5'
    dummy_file = '/home/obay/Dropbox/masterThesis/data/DUMMY.h5'

    start = time()
    data = get_h5_as_dict(input_file)
    print('Reading took ', time() - start)

    start = time()
    with h5py.File(dummy_file, 'w') as f:
        random_indices = np.random.permutation(data['Time'].size)
        for name, value in data.items():
            if name in ['cam1_Images', 'cam2_Images']:
                chunks = (128,) + tuple(value.shape[1:])
            else:
                chunks = value.shape

            f.create_dataset(name=name, shape=value.shape,
                             chunks=chunks, dtype=value.dtype,
                             compression='gzip', compression_opts=1,
                             data=value[random_indices])
    print('Writing took ', time() - start)


def benchmark_sequential_write():
    input_file = '/home/obay/Dropbox/masterThesis/data/single_files/proton_diffuse.lz4_128.h5'
    dummy_file = '/home/obay/Dropbox/masterThesis/data/DUMMY.h5'

    start = time()
    data = get_h5_as_dict(input_file)
    print('Reading took ', time()-start)

    start = time()
    with h5py.File(dummy_file, 'w') as f:
        for name, value in data.items():
            if name in ['cam1_Images', 'cam2_Images']:
                chunks = (128,) + tuple(value.shape[1:])
            else:
                chunks = value.shape

            f.create_dataset(name=name, shape=value.shape,
                             chunks=chunks, dtype=value.dtype,
                             compression='gzip', compression_opts=1,
                             data=value)
    print('Writing took ', time() - start)


def benchmark_random_read(method='list', size_cut=40., loc_dis_cut=0.525, multiplicity=2):

    input_folder = '/home/obay/Dropbox/masterThesis/data/new_exports/sandbox'
    file_paths = [os.path.join(input_folder, f) for f in os.listdir(input_folder)
                  if os.path.join(input_folder, f).endswith('.h5') and os.path.isfile(os.path.join(input_folder, f))]

    file_event_counts = []
    file_indices = []
    for file_path in file_paths:
        with h5py.File(file_path, 'r') as f:
            triggered_telescope_count = np.sum((f['cam1_size'].value > size_cut) &
                                               (f['cam1_loc_dis'].value < loc_dis_cut), axis=1)
            indices = np.arange(triggered_telescope_count.size)[triggered_telescope_count >= multiplicity]
            file_indices.append(indices.tolist())
            file_event_counts.append(len(indices))

    total_events = sum(file_event_counts)

    imgs = np.empty((total_events, 4, 960), dtype=np.float32)
    ids = np.empty((total_events, 5), dtype=np.int32)
    times = np.empty(total_events, dtype=np.float64)

    idx = 0
    if method == 'list':
        for file_path, indices in zip(file_paths, file_indices):
            with h5py.File(file_path, 'r') as f:
                imgs[idx:idx+len(indices)] = f['cam1_images'][indices]
                idx += len(indices)

    elif method == 'full':
        for file_path, indices in zip(file_paths, file_indices):
            with h5py.File(file_path, 'r') as f:
                imgs[idx:idx+len(indices)] = f['cam1_images'][:][indices]
                idx += len(indices)

    elif method == 'full_single':
        for file_path, indices in zip(file_paths, file_indices):
            with h5py.File(file_path, 'r') as f:
                for index in indices:
                    imgs[idx] = f['cam1_images'][index]
                    idx += 1

    random_indices = np.random.permutation(total_events)


if __name__ == '__main__':

    for cs in [1, 2, 3, 4, 5, 100, 500]:
        print('CHUNKSIZE: ', cs)
        repack_folder_to_lz4('/home/obay/Dropbox/masterThesis/data/new_exports/sandbox', cs)

        for method in ['full', 'full_single', 'list']:
            start = time()
            benchmark_random_read(method=method)
            print(method, ' ', time()-start)


    #benchmark_random_write()

    #benchmark_rw()

    #benchmark_chunked_read()

