from time import time
import numpy as np
import tensorflow as tf

NUM_ITEMS = 100000
BATCH_SIZE = 100


def gen():
    for x in np.arange(0, NUM_ITEMS, 1, dtype=np.int64):
        yield x


def gen_batch():
    for x in np.arange(0, NUM_ITEMS, 1, dtype=np.int64).reshape(NUM_ITEMS//BATCH_SIZE, BATCH_SIZE):
        yield x

start = time()
for _ in gen():
    pass
py_single_generator_time = time() - start

start = time()
for _ in gen_batch():
    pass
py_batch_generator_time = time() - start

ds_range_single = tf.data.Dataset.range(NUM_ITEMS)\
    .make_one_shot_iterator().get_next()

ds_range_batch = tf.data.Dataset.range(NUM_ITEMS)\
    .batch(BATCH_SIZE).make_one_shot_iterator().get_next()

ds_npy_single = tf.data.Dataset.from_generator(
    gen, output_types=tf.int64, output_shapes=tf.TensorShape([]))\
    .make_one_shot_iterator().get_next()

ds_npy_batch = tf.data.Dataset.from_generator(
    gen, output_types=tf.int64, output_shapes=tf.TensorShape([]))\
    .batch(BATCH_SIZE).make_one_shot_iterator().get_next()

ds_npy_single_batch = tf.data.Dataset.from_generator(
    gen_batch, output_types=tf.int64, output_shapes=tf.TensorShape([BATCH_SIZE]))\
    .make_one_shot_iterator().get_next()

with tf.Session() as sess:
    start = time()
    for _ in range(NUM_ITEMS):
        sess.run(ds_npy_single)
    npy_single_time = time() - start

    start = time()
    for _ in range(NUM_ITEMS // BATCH_SIZE):
        sess.run(ds_npy_batch)
    npy_batch_time = time() - start

    start = time()
    for _ in range(NUM_ITEMS // BATCH_SIZE):
        sess.run(ds_npy_single_batch)
    npy_single_batch_time = time() - start

    start = time()
    for _ in range(NUM_ITEMS):
        sess.run(ds_range_single)
    range_single_time = time() - start

    start = time()
    for _ in range(NUM_ITEMS // BATCH_SIZE):
        sess.run(ds_range_batch)
    range_batch_time = time() - start


print('Python single generator examples/s :', NUM_ITEMS/py_single_generator_time)
print('Python batch generator examples/s :', NUM_ITEMS/py_batch_generator_time)
print('tf npy single examples/s :', NUM_ITEMS/npy_single_time)
print('tf npy batch examples/s :', NUM_ITEMS/npy_batch_time)
print('tf npy single batch examples/s', NUM_ITEMS/npy_single_batch_time)
print('tf range single examples/s :', NUM_ITEMS/range_single_time)
print('tf range batch examples/s :', NUM_ITEMS/range_batch_time)