import input_pipeline.h5_generators as h5g
import input_pipeline.image_preprocessing as preproc
from time import time
import numpy as np
from tqdm import tqdm
import tensorflow as tf


file = '/home/obay/Dropbox/masterThesis/data/gamma_diffuse.lz4_128.h5'


for x in tqdm(h5g.SequentialH5Reader(file, chunk_size=128, preprocessing=None, yield_chunk=True).generator(), desc='raw'):
    pass

for x in tqdm(h5g.SequentialH5Reader(file, chunk_size=128, preprocessing=None, yield_chunk=False).generator(), desc='raw'):
    pass

for x in tqdm(h5g.SequentialH5Reader(file, chunk_size=128, rebin_resolution=32).generator(), desc='rebin 32'):
    pass

for x in tqdm(h5g.SequentialH5Reader(file, chunk_size=128, rebin_resolution=50).generator(), desc='rebin 50'):
    pass

for x in tqdm(h5g.SequentialH5Reader(file, chunk_size=128, rebin_resolution=100).generator(), desc='rebin 100'):
    pass