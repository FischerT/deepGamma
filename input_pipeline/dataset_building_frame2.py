import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import numpy as np
import os
import multiprocessing as mp
from itertools import chain, cycle
from time import time
from contextlib import ExitStack
from tqdm import tqdm
from itertools import islice
import input_pipeline.image_preprocessing as pp

data_format = {
    'id': [np.int64, [6]],
    'label': [np.int64, []],
    'cam1_images': [np.float32, [4, 960]],
    # 'cam2_images': [np.float32, [1, 2048]],
    'cam1_size': [np.float32, [4]],
    # 'cam2_size': [np.float32, [1]],
    'cam1_width': [np.float32, [4]],
    # 'cam2_width': [np.float32, [1]],
    'cam1_length': [np.float32, [4]],
    # 'cam2_length': [np.float32, [1]],
    'cam1_loc_dis': [np.float32, [4]],
    # 'cam2_loc_dis': [np.float32, [1]],
    'cam1_phi': [np.float32, [4]],
    # 'cam2_phi': [np.float32, [1]],
    'cam1_cog_x': [np.float32, [4]],
    # 'cam2_cog_x': [np.float32, [1]],
    'cam1_cog_y': [np.float32, [4]],
    # 'cam2_cog_y': [np.float32, [1]],
    'cam1_skewness': [np.float32, [4]],
    # 'cam2_skewness': [np.float32, [1]],
    'cam1_kurtosis': [np.float32, [4]],
    # 'cam2_kurtosis': [np.float32, [1]],
    'alt_az': [np.float32, [2]],
    'ra_dec': [np.float32, [2]],
    'core_ground': [np.float32, [3]],
    'core_tilted': [np.float32, [3]],
    'energy': [np.float32, []],
    'time': [np.float64, []],
    'nominal': [np.float32, [2]]
}


def cut_indices(file_path, size_cut, loc_dis_cut, multiplicity):
    with h5py.File(file_path) as f:
        triggered_telescope_count = np.sum((f['cam1_size'].value > size_cut) &
                                           (f['cam1_loc_dis'].value < loc_dis_cut), axis=1)
        indices = np.arange(triggered_telescope_count.size)[triggered_telescope_count >= multiplicity]
    return indices


class HESSDataset:
    def __init__(self, file_paths, indices):
        self.total_events = indices.shape[0]
        self.file_paths = file_paths
        self.indices = indices

    @classmethod
    def from_path(cls, base_path, size_cut, loc_dis_cut, multiplicity):
        # decide weather base path is file. If not fill file_list with files in folder
        if not os.path.exists(base_path):
            raise ValueError('There is no path like {}'.format(base_path))
        if os.path.isfile(base_path) and base_path.endswith('h5'):
            full_file_paths = [base_path]
        else:
            full_file_paths = []
            for _, _, file_paths in os.walk(base_path):
                for file_path in file_paths:
                    if file_path.endswith('.h5'):
                        full_path = os.path.join(base_path, file_path)
                        full_file_paths.append(full_path)
            if not full_file_paths:
                raise ValueError('There are no *.h5 files in  path {}'.format(base_path))

        # create file/event indices array (list of lists)
        event_indices = [cut_indices(file_path, size_cut, loc_dis_cut, multiplicity) for file_path in full_file_paths]
        file_indices = [[idx] * len(lst) for idx, lst in enumerate(event_indices)]

        # concatenate lists
        event_indices = list(chain(*event_indices))
        file_indices = list(chain(*file_indices))

        total_events = len(file_indices)

        indices = np.empty((total_events, 2), dtype=np.int64)
        indices[:, 0] = file_indices
        indices[:, 1] = event_indices

        return cls(full_file_paths, indices)

    @classmethod
    def concatenate(cls, a, b):
        file_paths = a.file_paths + b.file_paths
        b.indices[:, 0] += len(a.file_paths)
        indices = np.concatenate((a.indices, b.indices))
        return cls(file_paths, indices)

    @classmethod
    def interleave(cls, a, b):
        if a.total_events != b.total_events:
            raise ValueError('Event count does not match! {} vs {}'.format(a.total_events, b.total_events))
        file_paths = a.file_paths + b.file_paths
        b.indices[:, 0] += len(a.file_paths)
        indices = np.empty((a.total_events + b.total_events, 2), dtype=np.int64)
        indices[0::2] = a.indices
        indices[1::2] = b.indices
        return cls(file_paths, indices)

    def split(self, train_ratio=0.6, test_ratio=0.5, total_events=0):
        if total_events > self.total_events:
            raise ValueError('There are not enough events in this dataset')
        train_events = int(total_events * train_ratio)
        valid_events = int(total_events * (1 - train_ratio) * test_ratio)
        test_events = int(total_events * (1 - train_ratio) * (1 - test_ratio))

        valid_index = valid_events + train_events
        test_index = valid_index + test_events

        train_indices = self.indices[0: train_events]
        valid_indices = self.indices[train_events:valid_index]
        test_indices = self.indices[valid_index:total_events]

        return [HESSDataset(self.file_paths, indices) for indices in [train_indices, valid_indices, test_indices]]

    def shuffle(self):
        self.indices = np.random.permutation(self.indices)

    def store(self, file_path, n_buffers=3, buffer_size=6400):
        with h5py.File(file_path, 'w') as f:
            for name, props in data_format.items():
                f.create_dataset(name, shape=(self.total_events,) + tuple(props[1]),
                                 maxshape=(None,) + tuple(props[1]), dtype=props[0],
                                 chunks=(128,) + tuple(props[1]),
                                 compression=32004, shuffle=True)

            large_fields = ['cam1_images']  #, 'cam2_images']
            large_data_format = {name: props for name, props in data_format.items() if name in large_fields}
            small_data_format = {name: props for name, props in data_format.items() if name not in large_fields}

            buffers = []
            np_buffers = []
            processes = []

            for name, props in small_data_format.items():
                item_size = np.dtype(props[0]).itemsize
                event_size = int(np.prod(props[1]) * item_size)

                buffers.append(mp.Array('b', event_size * self.total_events, lock=False))
                np_buffers.append(np.frombuffer(buffers[-1], props[0]).reshape(-1, *props[1]))

                processes.append(mp.Process(target=self.work_small,
                                            args=(buffers[-1], name, props)))

            processes[0].start()
            for idx, (name, props) in enumerate(small_data_format.items()):
                if idx+1 < len(processes):
                    processes[idx+1].start()
                processes[idx].join()
                f[name][:] = np_buffers[idx]


            worker_indices = [self.indices[i:i + buffer_size] for i in range(0, self.total_events, buffer_size)]
            last_full_buffer = self.total_events - self.total_events % buffer_size
            worker_indices.append(self.indices[last_full_buffer:self.total_events])

            for name, props in large_data_format.items():
                item_size = np.dtype(props[0]).itemsize
                event_size = int(np.prod(props[1]) * item_size)

                buffers = []
                np_buffers = []
                processes = []
                buffer_slices = []
                for idx in range(n_buffers):
                    buffers.append(mp.Array('b', event_size * buffer_size, lock=False))
                    np_buffers.append(np.frombuffer(buffers[idx], props[0]).reshape(-1, *props[1]))

                    buffer_slices.append(islice(worker_indices, idx, None, n_buffers))

                    try:
                        processes.append(mp.Process(target=self.work_large,
                                                    args=(buffers[idx], next(buffer_slices[idx]), name, props)))
                        processes[idx].start()
                    except StopIteration:
                        pass

                lower = 0
                for idx, upper in zip(cycle(range(n_buffers)),
                                      chain(range(buffer_size, self.total_events, buffer_size), (self.total_events, ))):
                    processes[idx].join()
                    event_count = upper - lower
                    f[name][lower:upper] = np_buffers[idx][:event_count]
                    lower = upper
                    try:
                        processes[idx] = mp.Process(target=self.work_large,
                                                    args=(buffers[idx], next(buffer_slices[idx]), name, props))
                        processes[idx].start()
                    except StopIteration:
                        pass

    def work_large(self, buffer, indices, name, props):
        np_buffer = np.frombuffer(buffer, props[0]).reshape(-1, *props[1])
        with ExitStack() as stack:
            files = [stack.enter_context(h5py.File(fpath)) for fpath in self.file_paths]
            for idx, index in enumerate(indices):
                np_buffer[idx] = files[index[0]][name][index[1]]

    def work_small(self, buffer, name, props):
        np_buffer = np.frombuffer(buffer, props[0]).reshape(-1, *props[1])
        with ExitStack() as stack:
            files = [stack.enter_context(h5py.File(fpath)) for fpath in self.file_paths]
            np_files = [file[name][:] for file in files]
            for idx, index in enumerate(self.indices):
                np_buffer[idx] = np_files[index[0]][index[1]]

    def store_sparse(self, file_path, n_workers=56):
        small_data_format = {name: props for name, props in data_format.items() if name != 'cam1_images'}
        data = {}

        buffers = []
        np_buffers = []
        processes = []

        print('Reading small data')

        for name, props in small_data_format.items():
            item_size = np.dtype(props[0]).itemsize
            event_size = int(np.prod(props[1]) * item_size)

            buffers.append(mp.Array('b', event_size * self.total_events, lock=False))
            np_buffers.append(np.frombuffer(buffers[-1], props[0]).reshape(-1, *props[1]))

            processes.append(mp.Process(target=self.work_small,
                                        args=(buffers[-1], name, props)))

            processes[-1].start()

        for idx, name in enumerate(small_data_format.keys()):
            processes[idx].join()
            data[name] = np_buffers[idx]

        print('Finished reading small data')

        cam1_output_shape = pp.get_preprocessed_shape('rebin', 32)
        item_size = np.dtype(np.float32).itemsize
        event_size = int(np.prod([4, *cam1_output_shape]) * item_size)
        buffer = mp.Array('b', event_size * self.total_events, lock=False)

        processes = []
        for x in range(n_workers):
            p = mp.Process(target=self.work_hard,
                           args=(buffer, x, n_workers, cam1_output_shape))
            p.start()
            processes.append(p)

        print('reading images')

        for idx, p in enumerate(processes):
            print('joining', idx)
            p.join()
            print('joined', idx)

        print('done reading images')

        np_images = np.frombuffer(buffer, np.float32).reshape(-1, 4, *cam1_output_shape)
        print('images shape:', np_images.shape)
        print('sparsifying data')

        lengths = np.count_nonzero(np_images, axis=(1, 2, 3))
        idxs = np.flatnonzero(np_images)
        vals = np_images.ravel()[idxs]
        shape = np_images.shape

        print('storing data')

        data['lengths'] = lengths
        data['idxs'] = idxs
        data['vals'] = vals
        data['shape'] = shape

        np.savez(file_path, **data)

    def work_hard(self, buffer, worker_idx, n_workers, cam1_output_shape):
        print('worker started', worker_idx)
        indices = self.indices[worker_idx::n_workers]
        np_buffer = np.frombuffer(buffer, np.float32).reshape(-1, 4, *cam1_output_shape)
        interim_values = np.empty((len(indices), 4, 960), dtype=np.float32)
        with ExitStack() as stack:
            files = [stack.enter_context(h5py.File(fpath)) for fpath in self.file_paths]
            for idx, index in enumerate(indices):
                interim_values[idx] = files[index[0]]['cam1_images'][index[1]]
        print('worker done reading', worker_idx)
        np_buffer[worker_idx::n_workers] = pp.preprocess_cam1(interim_values, 'rebin', 32)
        print('worker done setting_data', worker_idx)


if __name__ == '__main__':
    global_start = time()

    start = time()
    protons = HESSDataset.from_path('/home/obay/Dropbox/masterThesis/data/new_exports/konrad_proton_diffuse')
    print('collect', protons.total_events ,'protons:', time()-start)

    start = time()
    gammas = HESSDataset.from_path('/home/obay/Dropbox/masterThesis/data/new_exports/konrad_gamma_diffuse')
    print('collect', gammas.total_events, 'gammas:', time() - start)

    start = time()
    protons.shuffle()
    print('shuffle protons:', time()-start)

    start = time()
    gammas.shuffle()
    print('shuffle gammas:', time()-start)

    start = time()
    gammas.store('/home/obay/Dropbox/masterThesis/data/new_exports/DUMMY.h5')
    print('storing:', time()-start)


