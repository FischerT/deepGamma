import numpy as np
from scipy.sparse import csr_matrix

import Polygon.Shapes as ps
from Polygon.cPolygon import Polygon

import os

import utilities.pixmaps as pixmaps

from functools import lru_cache

matrix_path_proto = os.path.join(os.path.dirname(__file__), 'preprocessing_matrices',  'rebinMatrix_{}.npz')
square_path_proto = os.path.join(os.path.dirname(__file__), 'preprocessing_matrices',  'squareGrid_{}.npz')


def get_preprocessed_shape(preprocessing, x_resolution):
    """
    Return the shape of the array returned by that preprocessing method

    :param preprocessing:
    :param x_resolution:
    :return:
    """
    if preprocessing == 'rebin':
        _, x_resolution = _get_rebin_matrix_and_resolution(x_resolution)
        return x_resolution
    elif preprocessing == 'hex':
        raise NotImplementedError
    elif preprocessing == 'linear':
        pass #todo
    elif preprocessing == 'cubic':
        pass #todo
    else:
        return (960, )

def get_preprocessing_grid(preprocessing, x_resolution):
    """
    
    :param preprocessing: 
    :param x_resolution: 
    :return: 
    """
    if preprocessing == 'rebin':
        matrix_path = matrix_path_proto.format(x_resolution)
        if not os.path.exists(matrix_path):
            _calculate_rebin_matrix(x_resolution)
        storage_dict = np.load(matrix_path)
        return storage_dict['grid']
    elif preprocessing == 'hex':
        raise NotImplementedError
    elif preprocessing == 'linear':
        pass # todo
    elif preprocessing == 'cubic':
        pass # todo
    else:
        raise ValueError

def preprocess_cam1(image_batch, preprocessing, x_resolution):
    """
    Preprocess an image batch with some desired method

    :param image_batch:
    :param preprocessing:
    :param x_resolution:
    :return:
    """
    if preprocessing == 'rebin':
        matrix, resolution = _get_rebin_matrix_and_resolution(x_resolution)
        image_batch = matrix.dot(image_batch.reshape(-1, 960).T).T.reshape(-1, image_batch.shape[1], *resolution)
        return image_batch
    elif preprocessing == 'hex':
        raise NotImplementedError
    elif preprocessing == 'linear':
        pass  # todo
    elif preprocessing == 'cubic':
        pass  # todo
    else:
        return image_batch

#region rebinning
@lru_cache(maxsize=10)  # store 10 last matrices in memory to get rid of IO bound
def _get_rebin_matrix_and_resolution(x_resolution):
    """
    If already computed, load rebin matrix and resolution. If not, compute!

    :param x_resolution: desired resolution of output images in x-direction
    :return: sparse rebin matrix, new image resolution
    """
    matrix_path = matrix_path_proto.format(x_resolution)
    if not os.path.exists(matrix_path):
        _calculate_rebin_matrix(x_resolution)
    storage_dict = np.load(matrix_path)
    rebin_matrix = csr_matrix((storage_dict['data'],
                               storage_dict['indices'],
                               storage_dict['indptr']))
    return rebin_matrix, storage_dict['resolution']


def _calculate_rebin_grid(camera_pixels, scale):
    """
    Helper method to calculate a grid for the new square pixels.

    :param camera_pixels: list of polygons
    :return: square grid coords, square grid resolution
    """
    # get basic geometry
    bounding_boxes = np.array([np.array(p.boundingBox()) for p in camera_pixels])
    xmin, ymin = np.amin(bounding_boxes[:, 0::2], axis=0)
    xmax, ymax = np.amax(bounding_boxes[:, 1::2], axis=0)
    asp_box = (xmax - xmin) / (ymax - ymin)
    pixel_count = len(camera_pixels)

    # calculate square grid resolution based on scaling parameter
    xres = (np.rint(scale * np.sqrt(pixel_count * asp_box))).astype(int)
    yres = (np.rint(scale * np.sqrt(pixel_count / asp_box))).astype(int)

    # match the aspect ratio
    asp_grid = xres / yres
    if asp_box > asp_grid:  # box wider than grid
        factor = asp_box / asp_grid
        dy = (ymax - ymin) * (factor - 1)
        ymin -= dy / 2
        ymax += dy / 2
    else:  # box higher than grid
        factor = asp_grid / asp_box
        dx = (xmax - xmin) * (factor - 1)
        xmin -= dx / 2
        xmax += dx / 2

    # setup the square grid
    square_distance = (xmax - xmin) / xres
    square_distance *= 1.000000000000001  # numerical thing, yo know
    squares = np.mgrid[xmin:xmax:square_distance, ymin:ymax:square_distance].reshape(2, -1).astype(np.float64)

    # return centers of new pixels, not bottom left corner
    return squares + square_distance / 2, (xres, yres)


def _calculate_rebin_matrix(x_resolution):
    pixmap = pixmaps.Cam1_mc_pixmap

    # get camera plane geometry
    pixel_distance = pixmap[0, 1] - pixmap[0, 0]
    hexagon_radius = pixel_distance * np.tan(np.pi / 6)
    pixel = ps.Circle(radius=hexagon_radius, points=6)
    pixel_area = pixel.area()

    # create list of all camera pixel polygons
    camera_pixels = []
    for pos in np.transpose(pixmap):
        pix = Polygon(pixel)
        pix.shift(pos[0], pos[1])
        camera_pixels.append(pix)

    # brute force lots of scale factors to find one matching the resolution
    for scale in np.arange(1., 4., 0.01, dtype=np.float32):
        rebin_grid, resolution = _calculate_rebin_grid(camera_pixels, scale)
        if resolution[0] == x_resolution:
            break
    else:
        raise ValueError("Could not find rebin matrix for resolution {}".format(x_resolution))

    square_distance = rebin_grid[1, 1] - rebin_grid[1, 0]

    # create list of all rebin pixel polygons
    square_pixels = []
    for coord in np.transpose(rebin_grid):
        square = ps.Rectangle(square_distance)
        square.shift(coord[0] - square_distance / 2, coord[1] - square_distance / 2)
        square_pixels.append(square)

    rebin_matrix = np.zeros((len(square_pixels), len(camera_pixels)))
    for idx in np.ndindex(rebin_matrix.shape):
        overlap = square_pixels[idx[0]] & camera_pixels[idx[1]]
        rebin_matrix[idx] = overlap.area() / pixel_area

    rebin_matrix = csr_matrix(rebin_matrix)

    #TODO: return matrix, do storing in _get_rebin_matrix_and_resolution()
    matrix_path = matrix_path_proto.format(x_resolution)
    np.savez(matrix_path,
             data=rebin_matrix.data,
             indices=rebin_matrix.indices,
             indptr=rebin_matrix.indptr,
             resolution=resolution,
             grid=rebin_grid)
#endregion

#region linear

@lru_cache(10)
def _calculate_square_grid(x_resolution):
    pixmap = pixmaps.Cam1_mc_pixmap
    dimensions = np.amax(pixmap, axis=1) - np.amin(pixmap, axis=1)
    asp = dimensions[0] / dimensions[1]
    pixel_count = pixmap.size / 2.
    # calculate square grid resolution based on scaling parameter

    for scale in np.arange(1., 4., 0.01, dtype=np.float32):
        xres = (np.rint(scale * np.sqrt(pixel_count * asp))).astype(int)
        if xres == x_resolution:
            break
    else:
        raise ValueError("Could not find rebin matrix for resolution {}".format(x_resolution))

    yres = (np.rint(scale * np.sqrt(pixel_count / asp))).astype(int)
    # setup the square grid
    squares = np.mgrid[0:xres, 0:yres].reshape(2, -1).astype(np.float64)
    # shift grid to center of mass = (0,0)
    squares -= np.mean(squares, axis=1, keepdims=True)
    # rescale grid to fit the camera dimensions
    squares /= np.amin(np.ptp(squares, axis=1) / dimensions)
    # save for later use
    square_path = square_path_proto.format(x_resolution)
    np.savez(square_path,
             grid=squares,
             resolution=(xres, yres))

#endregion

def calculate_hex_matrices():
    pass
