import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import numpy as np
import os
import uuid
import multiprocessing as mp
from itertools import chain, cycle
from time import time
import shutil
import input_pipeline.image_preprocessing as pp


data_format = {
    'id': [np.int64, [6]],
    'label': [np.int64, []],
    'cam1_images': [np.float32, [4, 960]],
    'cam2_images': [np.float32, [1, 2048]],
    'cam1_size': [np.float32, [4]],
    'cam2_size': [np.float32, [1]],
    'cam1_width': [np.float32, [4]],
    'cam2_width': [np.float32, [1]],
    'cam1_length': [np.float32, [4]],
    'cam2_length': [np.float32, [1]],
    'cam1_loc_dis': [np.float32, [4]],
    'cam2_loc_dis': [np.float32, [1]],
    'cam1_phi': [np.float32, [4]],
    'cam2_phi': [np.float32, [1]],
    'cam1_cog_x': [np.float32, [4]],
    'cam2_cog_x': [np.float32, [1]],
    'cam1_cog_y': [np.float32, [4]],
    'cam2_cog_y': [np.float32, [1]],
    'cam1_skewness': [np.float32, [4]],
    'cam2_skewness': [np.float32, [1]],
    'cam1_kurtosis': [np.float32, [4]],
    'cam2_kurtosis': [np.float32, [1]],
    'alt_az': [np.float32, [2]],
    'ra_dec': [np.float32, [2]],
    'core_ground': [np.float32, [3]],
    'core_tilted': [np.float32, [3]],
    'energy': [np.float32, []],
    'time': [np.float64, []]
}


class HESSDataset:
    def __init__(self, base_path, size_cut, loc_dis_cut, multiplicity):
        self.size_cut = size_cut
        self.loc_dis_cut = loc_dis_cut
        self.multiplicity = multiplicity

        if not os.path.exists(base_path):
            raise ValueError('There is no path like {}'.format(base_path))

        if not os.path.isfile(base_path):
            temp_folder = os.path.join('/tmp', str(uuid.uuid4()))
            while os.path.exists(temp_folder):
                temp_folder = os.path.join('/tmp', str(uuid.uuid4()))

            os.mkdir(temp_folder)
            self.file_path = os.path.join(temp_folder, 'all.h5x')

            self.folder_path = base_path
            self.total_events = 0
            self.file_paths = []
            self.files_indices = []
            self.collect_filter_indices()

        else:
            self.file_path = base_path
            self.file_paths = [base_path]
            self.files_indices = []
            self.collect_file_filter_indices()



    def collect_file_filter_indices(self):
        with h5py.File(self.file_paths[0], 'r') as f:
            triggered_telescope_count = np.sum((f['cam1_size'].value > self.size_cut) &
                                               (f['cam1_loc_dis'].value < self.loc_dis_cut), axis=1)
            indices = np.arange(triggered_telescope_count.size)[triggered_telescope_count >= self.multiplicity]
            self.files_indices.append(indices)

        self.total_events = sum([len(x) for x in self.files_indices])

    def collect_filter_indices(self):
        self.file_paths = [os.path.join(self.folder_path, f) for f in os.listdir(self.folder_path)
                      if os.path.join(self.folder_path, f).endswith('.h5') and
                      os.path.isfile(os.path.join(self.folder_path, f))]

        for file_path in self.file_paths:
            with h5py.File(file_path, 'r') as f:
                triggered_telescope_count = np.sum((f['cam1_size'].value > self.size_cut) &
                                                   (f['cam1_loc_dis'].value < self.loc_dis_cut), axis=1)
                indices = np.arange(triggered_telescope_count.size)[triggered_telescope_count >= self.multiplicity]
                self.files_indices.append(indices)

        self.total_events = sum([len(x) for x in self.files_indices])

    def gather_random_subset(self, n_events, n_buffers=2):
        if n_events > self.total_events:
            raise ValueError('There are not enough events (%i)! Requested %i' % (self.total_events, n_events))


        random_indices = np.random.permutation(self.total_events)[:n_events]

        file_numbers = []
        all_indices = []
        for file_number, file_indices in enumerate(self.files_indices):
            file_numbers.extend([file_number]*len(file_indices))
            all_indices.extend(file_indices)

        file_numbers = [file_numbers[idx] for idx in random_indices]
        all_indices = [all_indices[idx] for idx in random_indices]

        subset_files_indices = [[] for _ in self.files_indices]
        for file_number, file_index in zip(file_numbers, all_indices):
            subset_files_indices[file_number].append(file_index)

        with h5py.File(self.file_path, mode='w') as dest_file:
            for name, props in data_format.items():
                dest_file.create_dataset(name, shape=(n_events,) + tuple(props[1]), maxshape=(None,) + tuple(props[1]),
                                         dtype=props[0],
                                         chunks=(128,) + tuple(props[1]), compression=32004, shuffle=True)

        buffer_size = max([len(file_indices) for file_indices in subset_files_indices])
        # print(min([len(file_indices) for file_indices in subset_files_indices]), buffer_size)

        buffers = []
        np_buffers = []
        go_queues = []
        done_queues = []
        filler_processes = []
        for idx in range(n_buffers):
            marr_dic = {}
            arr_dic = {}
            for name, props in data_format.items():
                item_size = np.dtype(props[0]).itemsize
                event_size = int(np.prod(props[1]) * item_size)
                marr_dic[name] = mp.Array('b', event_size * buffer_size, lock=False)
                arr_dic[name] = np.frombuffer(marr_dic[name], props[0]).reshape(-1, *props[1])
            buffers.append(marr_dic)
            np_buffers.append(arr_dic)

            go_queues.append(mp.Queue(1))
            done_queues.append(mp.Queue(1))
            filler_processes.append(mp.Process(target=self.shmem_filler,
                                               args=(go_queues[idx], done_queues[idx], buffers[idx],
                                                     self.file_paths[idx::n_buffers],
                                                     subset_files_indices[idx::n_buffers])))
            filler_processes[idx].start()
            go_queues[idx].put(0)

        with h5py.File(self.file_path, mode='r+') as dest_file:
            start_event = 0
            for idx, _ in zip(cycle(range(n_buffers)), self.file_paths):
                events_from_file = done_queues[idx].get()
                last_event = events_from_file + start_event
                for name in data_format.keys():
                    dest_file[name][start_event:last_event] = np_buffers[idx][name][:events_from_file]
                go_queues[idx].put(None)
                start_event += events_from_file

        self.total_events = n_events


    def shmem_filler(self, go_queue, done_queue, marr_dic, file_paths, files_indices):
        arr_dic = {}
        for name, props in data_format.items():
            arr_dic[name] = np.frombuffer(marr_dic[name], props[0]).reshape(-1, *props[1])

        for file_path, file_indices in zip(file_paths, files_indices):
            with h5py.File(file_path, 'r') as f:
                go_queue.get()

                for name, props in data_format.items():
                    arr_dic[name][:len(file_indices)] = f[name].value[file_indices]

                done_queue.put(len(file_indices))

    def split(self, train_ratio=0.6, test_ratio=0.5, total_events=0):
        if total_events > self.total_events:
            raise ValueError('There are not enough events in this dataset')
        train_events = int(total_events * train_ratio)
        valid_events = int(total_events * (1 - train_ratio) * test_ratio)
        test_events = int(total_events * (1 - train_ratio) * (1-test_ratio))

        indices = np.arange(total_events)

        set_indices = {
            'train': indices[:train_events],
            'valid': indices[train_events: train_events + valid_events],
            'test': indices[train_events + valid_events:]
        }

        with h5py.File(self.file_path, 'r') as src:
            for set_name, indices in set_indices.items():
                with h5py.File(self.file_path+set_name, 'w') as dest:
                    for name, props in data_format.items():
                        dest.create_dataset(name, shape=(indices.size,)+tuple(props[1]),
                                            maxshape=(None,)+tuple(props[1]), dtype=props[0], chunks=(128,)+tuple(props[1]),
                                            compression=32004, shuffle=True, data=src[name].value[indices])

        return [HESSDataset(self.file_path+set_name, self.size_cut, self.loc_dis_cut, self.multiplicity)
                for set_name in set_indices.keys()]

    def shuffle(self):
        with h5py.File(self.file_path, 'r+') as f:
            total_events = f['time'].size
            indices = np.random.permutation(total_events)
            for name in data_format.keys():
                f[name][:] = f[name].value[indices]

    def concatenate(self, other):
        with h5py.File(self.file_path, 'r+') as dest:
            with h5py.File(other.file_path, 'r') as src:
                for name, props in data_format.items():
                    new_size = self.total_events + other.total_events
                    dest[name].resize(new_size, axis=0)
                    dest[name][self.total_events:new_size] = src[name]

        self.total_events += other.total_events

    def interleave(self, other):
        if self.total_events != other.total_events:
            raise ValueError('Unequal event counts, cannot interleave!')

        temp_folder = os.path.join('/tmp', str(uuid.uuid4()))
        while os.path.exists(temp_folder):
            temp_folder = os.path.join('/tmp', str(uuid.uuid4()))
        os.mkdir(temp_folder)

        with h5py.File(temp_folder+'/all.h5x', 'w') as dest:
            with h5py.File(self.file_path, 'r') as src1, h5py.File(other.file_path, 'r') as src2:
                for name, props in data_format.items():
                    dest.create_dataset(name, shape=(self.total_events*2,) + tuple(props[1]),
                                        maxshape=(None,) + tuple(props[1]), dtype=props[0],
                                        chunks=(128,) + tuple(props[1]), compression=32004, shuffle=True)
                    buffer = np.empty((100000,) + tuple(props[1]), dtype=props[0])
                    lbound=0
                    for ubound in chain(range(50000, self.total_events, 50000), (self.total_events,)):
                        buffer[0:(ubound-lbound)*2:2] = src1[name][lbound:ubound]
                        buffer[1:(ubound-lbound)*2:2] = src2[name][lbound:ubound]
                        dest[name][lbound*2:ubound*2] = buffer[:(ubound-lbound)*2]
                        lbound = ubound

        return HESSDataset(temp_folder+'/all.h5x', self.size_cut, self.loc_dis_cut, self.multiplicity)

    def move(self, destination_path):
        shutil.move(self.file_path, destination_path)
