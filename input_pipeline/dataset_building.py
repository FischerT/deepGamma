import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import numpy as np
import os
from scipy import sparse as sp
from tqdm import tqdm
from time import time
import multiprocessing as mp
from itertools import repeat, cycle
import shutil


data_format = {
    'id': [np.int64, [5]],
    'label': [np.int64, []],
    'cam1_images': [np.float32, [4, 960]],
    'cam2_images': [np.float32, [1, 2048]],
    'cam1_size': [np.float32, [4]],
    'cam2_size': [np.float32, [1]],
    'cam1_width': [np.float32, [4]],
    'cam2_width': [np.float32, [1]],
    'cam1_length': [np.float32, [4]],
    'cam2_length': [np.float32, [1]],
    'cam1_loc_dis': [np.float32, [4]],
    'cam2_loc_dis': [np.float32, [1]],
    'cam1_phi': [np.float32, [4]],
    'cam2_phi': [np.float32, [1]],
    'cam1_cog_x': [np.float32, [4]],
    'cam2_cog_x': [np.float32, [1]],
    'cam1_cog_y': [np.float32, [4]],
    'cam2_cog_y': [np.float32, [1]],
    'cam1_skewness': [np.float32, [4]],
    'cam2_skewness': [np.float32, [1]],
    'cam1_kurtosis': [np.float32, [4]],
    'cam2_kurtosis': [np.float32, [1]],
    'alt_az': [np.float32, [2]],
    'ra_dec': [np.float32, [2]],
    'core_ground': [np.float32, [3]],
    'core_tilted': [np.float32, [3]],
    'energy': [np.float32, []],
    'time': [np.float64, []]
}


def get_file_data_format(file_path):
    with h5py.File(file_path, 'r') as f:
        return {name: [f[name].dtype, f[name].shape[1:]] for name in f.keys()}


def repack_file_to_lz4(file_path):
    data = {}
    data_shape = {}
    data_maxshape = {}
    data_dtype = {}
    data_chunks = {}

    with h5py.File(file_path, 'r') as f:
        for name, value in data_format.items():
            data[name] = f[name].value
            data_shape[name] = f[name].shape
            data_maxshape[name] = f[name].maxshape
            data_dtype[name] = f[name].dtype
            data_chunks[name] = f[name].chunks

    with h5py.File(file_path, 'w') as f:
        for name, value in data_format.items():
            f.create_dataset(name, shape=data_shape[name], maxshape=data_maxshape[name], dtype=data_dtype[name],
                             chunks=data_chunks[name], compression=32004, shuffle=True, data=data[name])


def repack_folder_to_lz4(folder_path):
    file_paths = [os.path.join(folder_path, f) for f in os.listdir(folder_path)
                  if os.path.join(folder_path, f).endswith('.h5') and os.path.isfile(os.path.join(folder_path, f))]
    with mp.Pool(os.cpu_count(), maxtasksperchild=1) as p:
        p.map(repack_file_to_lz4, file_paths)


def queue_filtered_data_dict(queue, file_path, size_cut, multiplicity):
    data = {}
    with h5py.File(file_path, 'r') as f:
        triggered_telescope_count = np.sum((f['cam1_size'].value > size_cut), axis=1)
        indices = np.arange(triggered_telescope_count.size)[triggered_telescope_count >= multiplicity]

        for name, value in data_format.items():
            data[name] = f[name].value[indices]
    queue.put(data)


def fill_queue_with_folder_data(queue, folder_path, size_cut, multiplicity):
    src_file_paths = [os.path.join(folder_path, f) for f in os.listdir(folder_path)
                      if os.path.join(folder_path, f).endswith('.h5') and os.path.isfile(os.path.join(folder_path, f))]
    with mp.Pool(2, maxtasksperchild=1) as p:
        p.starmap(queue_filtered_data_dict, zip(repeat(queue), src_file_paths, repeat(size_cut), repeat(multiplicity)))
    queue.put(None)


def unite_all_files_in_folder(folder_path, size_cut, multiplicity):
    with h5py.File(os.path.join(folder_path, 'all.h5x'), mode='w') as dest_file:
        for name, props in data_format.items():
            dest_file.create_dataset(name, shape=(0,)+tuple(props[1]), maxshape=(None,)+tuple(props[1]), dtype=props[0],
                                     chunks=(128,)+tuple(props[1]), compression=32004, shuffle=True)

        m = mp.Manager()
        data_queue = m.Queue(2)
        p_filler = mp.Process(target=fill_queue_with_folder_data, args=(data_queue, folder_path, size_cut, multiplicity))
        p_filler.start()

        for src_data in iter(data_queue.get, None):
            for name in data_format.keys():
                current_size = dest_file[name].shape[0]
                new_size = current_size + src_data[name].shape[0]
                dest_file[name].resize(new_size, axis=0)
                dest_file[name][current_size:new_size] = src_data[name][:]

        return dest_file['time'].size


def unite_all_files_in_folder_shmem(folder_path, loc_dis_cut, size_cut, multiplicity, n_buffers=2):
    with h5py.File(os.path.join(folder_path, 'all.h5x'), mode='w') as dest_file:
        for name, props in data_format.items():
            dest_file.create_dataset(name, shape=(0,)+tuple(props[1]), maxshape=(None,)+tuple(props[1]), dtype=props[0],
                                     chunks=(128,)+tuple(props[1]), compression=32004, shuffle=True)

        file_paths = [os.path.join(folder_path, f) for f in os.listdir(folder_path)
                      if os.path.join(folder_path, f).endswith('.h5') and os.path.isfile(os.path.join(folder_path, f))]

        buffer_size = int(get_largest_file_event_count_in_folder(folder_path))

        buffers = []
        np_buffers = []
        go_queues = []
        done_queues = []
        filler_processes = []
        for idx in range(n_buffers):
            marr_dic = {}
            for name, props in data_format.items():
                item_size = np.dtype(props[0]).itemsize
                event_size = int(np.prod(props[1]) * item_size)
                marr_dic[name] = mp.Array('b', event_size * buffer_size, lock=False)
            buffers.append(marr_dic)

            arr_dic = {}
            for name, props in data_format.items():
                arr_dic[name] = np.frombuffer(marr_dic[name], props[0]).reshape(-1, *props[1])
            np_buffers.append(arr_dic)

            go_queues.append(mp.Queue(1))
            done_queues.append(mp.Queue(1))
            filler_processes.append(mp.Process(target=shmem_filler, args=(go_queues[idx], done_queues[idx],
                                                                          buffers[idx], file_paths[idx::n_buffers],
                                                                          loc_dis_cut, size_cut, multiplicity)))
            filler_processes[idx].start()
            go_queues[idx].put(0)

        for idx, _ in zip(cycle(range(n_buffers)), file_paths):
            n_events = done_queues[idx].get()

            for name in data_format.keys():
                current_size = dest_file[name].shape[0]
                new_size = current_size + n_events
                dest_file[name].resize(new_size, axis=0)
                dest_file[name][current_size:new_size] = np_buffers[idx][name][:n_events]

            go_queues[idx].put(0)

        return dest_file['time'].size


def shmem_filler(go_queue, done_queue, marr_dic, file_paths, loc_dis_cut, size_cut, multiplicity):
    arr_dic = {}
    for name, props in data_format.items():
        arr_dic[name] = np.frombuffer(marr_dic[name], props[0]).reshape(-1, *props[1])

    for file_path in file_paths:
        with h5py.File(file_path, 'r') as f:
            triggered_telescope_count = np.sum((f['cam1_size'].value > size_cut) &
                                               (f['cam1_loc_dis'].value < loc_dis_cut), axis=1)
            indices = np.arange(triggered_telescope_count.size)[triggered_telescope_count >= multiplicity]
            n_events = indices.size
            go_queue.get()

            for name, props in data_format.items():
                arr_dic[name][:n_events] = f[name].value[indices]

            done_queue.put(n_events)


def get_largest_file_event_count_in_folder(folder_path):
    file_paths = [os.path.join(folder_path, f) for f in os.listdir(folder_path)
                  if os.path.join(folder_path, f).endswith('.h5') and os.path.isfile(os.path.join(folder_path, f))]

    largest = 0
    for file_path in file_paths:
        with h5py.File(file_path, 'r') as f:
            largest = max((f['time'].size,largest))

    return largest


def shuffle_file(file_path):
    with h5py.File(file_path, 'r+') as f:
        total_events = f['time'].size
        indices = np.random.permutation(total_events)
        for name in data_format.keys():
            f[name][:] = f[name].value[indices]


def split_file(file_path, train_ratio=0.6, test_ratio=0.5, total_events=164442):
    with h5py.File(file_path, 'r+') as f:
        print('eventcount: ', f['time'].size)

    train_events = int(total_events * train_ratio)
    valid_events = int(total_events * (1 - train_ratio) * test_ratio)
    test_events = int(total_events * (1 - train_ratio) * test_ratio)

    indices = np.arange(total_events)

    set_indices = {
        'train': indices[:train_events],
        'valid': indices[train_events: train_events + valid_events],
        'test': indices[train_events + valid_events:]
    }

    with h5py.File(file_path, 'r') as src:
        for set_name, indices in set_indices.items():
            with h5py.File(file_path+set_name, 'w') as dest:
                for name, props in data_format.items():
                    dest.create_dataset(name, shape=(indices.size,)+tuple(props[1]),
                                        maxshape=(None,)+tuple(props[1]), dtype=props[0], chunks=(128,)+tuple(props[1]),
                                        compression=32004, shuffle=True, data=src[name].value[indices])

    return [file_path + set_name for set_name in set_indices.keys()]


def interleave(destination_path, folder_paths):
    if not os.path.isdir(destination_path):
        os.mkdir(destination_path)

    set_names = ['train', 'valid', 'test']
    for set_name in set_names:
        event_counts = []
        for folder_path in folder_paths:
            with h5py.File(os.path.join(folder_path, 'all.h5x' + set_name), 'r') as src:
                event_counts.append(src['time'].size)

        if len(set(event_counts)) > 1:
            print('Unbalanced classes!')

        total_events = sum(event_counts)

        # fill files
        with h5py.File(os.path.join(destination_path, set_name + '.h5'), mode='w') as dest:
            for name, props in data_format.items():
                buffer = np.empty((total_events,) + tuple(props[1]), dtype=props[0])
                for idx, folder_path in enumerate(folder_paths):
                    with h5py.File(os.path.join(folder_path, 'all.h5x'+set_name), 'r') as src:
                        buffer[idx::len(folder_paths)] = src[name][:]
                dest.create_dataset(name, shape=(total_events,) + tuple(props[1]),
                                    maxshape=(None,) + tuple(props[1]), dtype=props[0],
                                    chunks=(128,) + tuple(props[1]), compression=32004, shuffle=True, data=buffer)


def concatenate(new_file, files):
    with h5py.File(new_file, 'w') as dest_file:
        for name, props in data_format.items():
            dest_file.create_dataset(name, shape=(0,)+tuple(props[1]), maxshape=(None,)+tuple(props[1]), dtype=props[0],
                                     chunks=(128,)+tuple(props[1]), compression=32004, shuffle=True)
            for file in files:
                with h5py.File(file, 'r') as src_file:
                    current_size = dest_file[name].shape[0]
                    new_size = current_size + src_file[name].shape[0]
                    dest_file[name].resize(new_size, axis=0)
                    dest_file[name][current_size:new_size] = src_file[name][:]


if __name__ == '__main__':
    proton_folder = '/home/obay/Dropbox/masterThesis/data/new_exports/konrad_proton_diffuse'

    global_start = time()



    start = time()
    unite_all_files_in_folder_shmem(proton_folder, 20., 2)
    print('unite protons smem', time() - start)

    start = time()
    unite_all_files_in_folder(proton_folder, 20., 2)
    print('unite protons', time() - start)