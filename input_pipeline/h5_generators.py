import sys
if not 'h5py' in sys.modules:
    import hdf5plugin
import h5py
import numpy as np
import tensorflow as tf
import input_pipeline.image_preprocessing as pp
from time import time
import multiprocessing as mp
from itertools import cycle, islice
import os

DATA_ROOT = os.environ['MASTERDATA']


data_format = {
    'id': [np.int64, [6]],
    'label': [np.int64, []],
    'cam1_images': [np.float32, [4, 960]],
    # 'cam2_images': [np.float32, [1, 2048]],
    'cam1_size': [np.float32, [4]],
    # 'cam2_size': [np.float32, [1]],
    'cam1_width': [np.float32, [4]],
    # 'cam2_width': [np.float32, [1]],
    'cam1_length': [np.float32, [4]],
    # 'cam2_length': [np.float32, [1]],
    'cam1_loc_dis': [np.float32, [4]],
    # 'cam2_loc_dis': [np.float32, [1]],
    'cam1_phi': [np.float32, [4]],
    # 'cam2_phi': [np.float32, [1]],
    'cam1_cog_x': [np.float32, [4]],
    # 'cam2_cog_x': [np.float32, [1]],
    'cam1_cog_y': [np.float32, [4]],
    # 'cam2_cog_y': [np.float32, [1]],
    'cam1_skewness': [np.float32, [4]],
    # 'cam2_skewness': [np.float32, [1]],
    'cam1_kurtosis': [np.float32, [4]],
    # 'cam2_kurtosis': [np.float32, [1]],
    'alt_az': [np.float32, [2]],
    'ra_dec': [np.float32, [2]],
    'core_ground': [np.float32, [3]],
    'core_tilted': [np.float32, [3]],
    'energy': [np.float32, []],
    'time': [np.float64, []],
    'nominal': [np.float32, [2]]
}

class NumpyReader:
    """

    wraps a generator for npz files containing sparse uncompressed data
    """

    def __init__(self, file_path, requested_batches=np.inf, batch_size=128, target=None):
        if target not in data_format.keys():
            raise ValueError('target does not exist: ', target)
        else:
            self.target = target

        self.file_path = file_path
        self.batch_size = batch_size

        with np.load(file_path) as f:
            self.total_events = f['shape'][0]
            self.total_batches = self.total_events // self.batch_size
            self.total_events = self.batch_size * self.total_batches

            self.lengths = f['lengths'][:self.total_events]
            self.idxs = f['idxs']
            self.vals = f['vals']
            self.shape = f['shape']
            self.targets = f[target]

        self.requested_batches = requested_batches or self.total_batches
        self.idxs %= np.prod([self.batch_size, *self.shape[1:]])
        self.lengths = np.sum(self.lengths.reshape(-1, self.batch_size), axis=1)
        self.upper_idx = np.cumsum(self.lengths)
        self.lower_idx = self.upper_idx-self.lengths

    def generator(self):
        total_batch_number = 0
        while total_batch_number < self.requested_batches:
            batch_number = total_batch_number % self.total_batches

            lower_idx = self.lower_idx[batch_number]
            upper_idx = self.upper_idx[batch_number]

            batch = np.zeros((self.batch_size, *self.shape[1:]), dtype=np.float32)
            batch.ravel()[self.idxs[lower_idx:upper_idx]] = self.vals[lower_idx:upper_idx]

            event_idx = batch_number * self.batch_size
            target = self.targets[event_idx:event_idx + self.batch_size]
            yield (batch, target)

            total_batch_number += 1



class SequentialH5Reader:
    """
    Wraps a generator of dataset batches(HDF jargon) aka batches(TF jargon).
    Preprocesses with a desired method.

    Parameters:
        file_path: String
        Full path of HDF5 file to be read

        batch_size: Integer = 128
        Number of rows read from HDF5 in one operations

        preprocessing: string = 'rebin' {'rebin' | 'hex' | None}
        Preprocessing method of choice

        rebin_resolution: int = 50
        resolution of output images
    """

    def __init__(self, file_path, start_batch=0, requested_batches=0, meta_batch_size=25,
                 batch_size=128, preprocessing='rebin', rebin_resolution=50, target=None):
        if target not in data_format.keys():
            raise ValueError('target does not exist: ', target)
        else:
            self.target = target

        self.file_path = file_path
        self.start_batch = start_batch
        self.batch_size = batch_size
        self.meta_batch_size = meta_batch_size
        self.preprocessing = preprocessing
        self.rebin_resolution = rebin_resolution

        with h5py.File(file_path, mode='r') as f:
            labels = f['label'][:]

            self.total_events = labels.size
            self.total_batches = self.total_events//self.batch_size
            self.total_events = self.batch_size * self.total_batches


        self.requested_batches = requested_batches or self.total_batches
        self.data_format = data_format.copy()
        self.cam1_output_shape = pp.get_preprocessed_shape(preprocessing, rebin_resolution)
        self.data_format['cam1_images_pp'] = [np.float32, [4, *self.cam1_output_shape]]

    def full_generator(self):
        data = {}
        with h5py.File(self.file_path, mode='r') as f:
            for name, props in data_format.items():
                data[name] = f[name][:]
            data['cam1_images_pp'] = pp.preprocess_cam1(data['cam1_images'], self.preprocessing, self.rebin_resolution)

        batch = 0
        while True:
            for x in range(self.batch_size, self.total_events+1, self.batch_size):
                yield (data['cam1_images_pp'][x-self.batch_size:x], data[self.target][x-self.batch_size:x])
                batch += 1
                if batch == self.requested_batches:
                    return

    def target_generator(self, n_buffers=4):
        for stuff in self.generator(n_buffers):
            yield (stuff['cam1_images_pp'], stuff[self.target])

    def generator(self, n_buffers=4):
        buffers = []
        np_buffers = []
        filler_processes = []
        buffer_slices = []
        for idx in range(n_buffers):
            marr_dic = {}
            for name, props in self.data_format.items():
                item_size = np.dtype(props[0]).itemsize
                event_size = int(np.prod(props[1]) * item_size)
                marr_dic[name] = mp.Array('b', event_size * self.batch_size * self.meta_batch_size, lock=False)
            buffers.append(marr_dic)

            arr_dic = {}
            for name, props in self.data_format.items():
                arr_dic[name] = np.frombuffer(marr_dic[name], props[0]).reshape(-1, *props[1])
            np_buffers.append(arr_dic)

            buffer_slices.append(islice(self.slices(), idx, None, n_buffers))

            try:  # the stop iteration raised by next() should not stop the outer loop.
                filler_processes.append(mp.Process(target=self.filler,
                                                   args=(buffers[idx], next(buffer_slices[idx]))))
                filler_processes[idx].start()
            except StopIteration:
                pass

        for idx, slc in zip(cycle(range(n_buffers)), self.slices()):
            filler_processes[idx].join()
            meta_batch_size = slc[1]-slc[0]
            for row in range(0, meta_batch_size, self.batch_size):
                yield {name: np_buffers[idx][name][row:row+self.batch_size] for name in self.data_format.keys()}
            try:  # the stop iteration raised by next() should not stop the outer loop.
                filler_processes[idx] = mp.Process(target=self.filler,
                                                   args=(buffers[idx], next(buffer_slices[idx])))
                filler_processes[idx].start()
            except StopIteration:
                pass

    def filler(self, marr_dic, slc):
        arr_dic = {}
        for name, props in self.data_format.items():
            arr_dic[name] = np.frombuffer(marr_dic[name], props[0]).reshape(-1, *props[1])

        with h5py.File(self.file_path, 'r') as f:
            meta_batch_size = slc[1]-slc[0]
            for name, props in data_format.items():
                f[name].read_direct(arr_dic[name], slice(*slc), slice(0, meta_batch_size))
            arr_dic['cam1_images_pp'][:meta_batch_size] = pp.preprocess_cam1(
                arr_dic['cam1_images'][:meta_batch_size], self.preprocessing, self.rebin_resolution)

    def get_tf_dataset(self):
        output_types = {name: tf.as_dtype(props[0]) for name, props in self.data_format.items()}
        tensor_shapes = {name: tf.TensorShape([self.batch_size, *props[1]]) for name, props in self.data_format.items()}
        return tf.data.Dataset.from_generator(generator=self.generator, output_types=output_types,
                                              output_shapes=tensor_shapes)

    def _fast_dummy_generator(self):
        data = {name: np.empty((self.batch_size, *props[1]), dtype=props[0])
                for name, props in self.data_format.items()}
        for _ in range(self.total_batches):
            yield data

    def _get_fast_dummy_tf_dataset(self):
        output_types = {name: tf.as_dtype(props[0]) for name, props in self.data_format.items()}
        tensor_shapes = {name: tf.TensorShape([self.batch_size, *props[1]]) for name, props in self.data_format.items()}
        return tf.data.Dataset.from_generator(generator=self._fast_dummy_generator, output_types=output_types,
                                              output_shapes=tensor_shapes)

    def slices(self):
        total_events = self.total_events - self.total_events % self.batch_size
        total_batches = total_events // self.batch_size
        requested_batches = self.requested_batches

        # lower and upper indices for metabatches
        lower_batch_idx = self.start_batch
        while True:
            # wraparound at end of dataset
            lower_batch_idx %= total_batches

            # calculate upper index for full metabatch
            upper_batch_idx = lower_batch_idx + self.meta_batch_size
            # truncate metabatch if at end of dataset
            upper_batch_idx = min(upper_batch_idx, total_batches)

            batches_in_batch = upper_batch_idx - lower_batch_idx
            requested_batches -= batches_in_batch

            # yield smaller last metabatch to not overshoot requested batches
            # this is also the end of the generator
            if requested_batches <= 0:
                upper_batch_idx += requested_batches  # requested batches is negative at this point!
                yield (lower_batch_idx * self.batch_size, upper_batch_idx * self.batch_size)
                break

            yield (lower_batch_idx * self.batch_size, upper_batch_idx * self.batch_size)
            lower_batch_idx += batches_in_batch

if __name__ == '__main__':
    npreader = NumpyReader(file_path=DATA_ROOT + 'regression/gamma_valid.npz', requested_batches=0,
                           batch_size=2048, target='label')

    start = time()
    for idx, stuff in enumerate(npreader.generator2()):
        pass
    dt = time() - start
    print("Total time: ", dt)
    print("batches per second: ", npreader.total_batches/dt)

